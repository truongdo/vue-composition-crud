import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import { mock, instance, when, verify, deepEqual } from "ts-mockito";
import { CrudApi } from "../api/types";
import { usePage } from "./page";

chai.use(chaiAsPromised);
const expect = chai.expect

interface SimpleItem {
    id?: number;
    name: string;
}


describe("the use page composable", () => {

    it("should have a blank initial state", (): void => {
        const mockApi: CrudApi = mock();
        const api = instance(mockApi);

        const { isUnloaded, isLoading, isLoaded, isFailed, error, page, items } = usePage<SimpleItem>(api, "item");

        expect(isUnloaded.value).to.be.true;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.be.undefined;
        expect(items.value).to.be.undefined;
    })


    it("should retrieve a collection of items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 1, undefined)).thenResolve(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: 2,
                    last: 2
                },
                items: [
                    { id: 1, name: "first" },
                    { id: 2, name: "second" },
                    { id: 3, name: "third" }
                ],
                meta: {}
            }
        )

        const { isUnloaded, isLoading, isLoaded, isFailed, error, page, items, listItems } = usePage<SimpleItem>(api, "item");

        const promise = listItems();
        await expect(promise).to.eventually.eql(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: 2,
                    last: 2
                },
                items: [
                    { id: 1, name: "first" },
                    { id: 2, name: "second" },
                    { id: 3, name: "third" }
                ],
                meta: {}
            }
        );

        verify(mockApi.page("item", 1, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 1,
                previous: null,
                next: 2,
                last: 2
            },
            items: [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])
    })



    it("should retrieve a collection of items from the second page", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 2, undefined)).thenResolve(
            {
                page: {
                    current: 2,
                    previous: 1,
                    next: null,
                    last: 2
                },
                items: [
                    { id: 4, name: "fourth" },
                    { id: 5, name: "fifth" },
                    { id: 6, name: "sixth" }
                ],
                meta: {}
            }
        )

        const { isUnloaded, isLoading, isLoaded, isFailed, error, page, items, listItems } = usePage<SimpleItem>(api, "item");

        const promise = listItems(2);
        await expect(promise).to.eventually.eql(
            {
                page: {
                    current: 2,
                    previous: 1,
                    next: null,
                    last: 2
                },
                items: [
                    { id: 4, name: "fourth" },
                    { id: 5, name: "fifth" },
                    { id: 6, name: "sixth" }
                ],
                meta: {}
            }
        );

        verify(mockApi.page("item", 2, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 2,
                previous: 1,
                next: null,
                last: 2
            },
            items: [
                { id: 4, name: "fourth" },
                { id: 5, name: "fifth" },
                { id: 6, name: "sixth" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 4, name: "fourth" },
            { id: 5, name: "fifth" },
            { id: 6, name: "sixth" }
        ])
    })


    it("should retrieve a collection of filtered items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 2, deepEqual({ search: "th" }))).thenResolve(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: null,
                    last: 1
                },
                items: [
                    { id: 3, name: "third" },
                    { id: 4, name: "fourth" },
                    { id: 5, name: "fifth" },
                    { id: 6, name: "sixth" }
                ],
                meta: {}
            }
        )

        const { isUnloaded, isLoading, isLoaded, isFailed, error, page, items, listItems } = usePage<SimpleItem>(api, "item");

        const promise = listItems(2, { search: "th" });
        await expect(promise).to.eventually.eql(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: null,
                    last: 1
                },
                items: [
                    { id: 3, name: "third" },
                    { id: 4, name: "fourth" },
                    { id: 5, name: "fifth" },
                    { id: 6, name: "sixth" }
                ],
                meta: {}
            }
        );

        verify(mockApi.page("item", 2, deepEqual({ search: "th" }))).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 1,
                previous: null,
                next: null,
                last: 1
            },
            items: [
                { id: 3, name: "third" },
                { id: 4, name: "fourth" },
                { id: 5, name: "fifth" },
                { id: 6, name: "sixth" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 3, name: "third" },
            { id: 4, name: "fourth" },
            { id: 5, name: "fifth" },
            { id: 6, name: "sixth" }
        ])
    })



    it("should fail to retrieve a collection of items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 1, undefined)).thenReject(new Error("Mock API Error"))

        const { isUnloaded, isLoading, isLoaded, isFailed, error, page, items, listItems } = usePage<SimpleItem>(api, "item");

        const promise = listItems(1);
        await expect(promise).to.eventually.be.rejectedWith("Mock API Error").and.be.an.instanceOf(Error);

        verify(mockApi.page("item", 1, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.true;
        expect((error.value as Error).message).to.equal("Mock API Error");
        expect(page.value).to.be.undefined;
        expect(items.value).to.be.undefined;
    })



    it("should reset after retrieving a collection of items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 1, undefined)).thenResolve(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: 2,
                    last: 2
                },
                items: [
                    { id: 1, name: "first" },
                    { id: 2, name: "second" },
                    { id: 3, name: "third" }
                ],
                meta: {}
            }
        )

        const { isUnloaded, isLoading, isLoaded, isFailed, error, page, items, reset, listItems } = usePage<SimpleItem>(api, "item");

        const promise = listItems();
        await expect(promise).to.eventually.eql(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: 2,
                    last: 2
                },
                items: [
                    { id: 1, name: "first" },
                    { id: 2, name: "second" },
                    { id: 3, name: "third" }
                ],
                meta: {}
            }
        );

        verify(mockApi.page("item", 1, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 1,
                previous: null,
                next: 2,
                last: 2
            },
            items: [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])

        await reset();

        expect(isUnloaded.value).to.be.true;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.be.undefined;
        expect(items.value).to.be.undefined;
    })

});

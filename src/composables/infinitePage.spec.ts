import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import { mock, instance, when, verify, deepEqual } from "ts-mockito";
import { CrudApi } from "../api/types";
import { useInfinitePage } from "./infinitePage";

chai.use(chaiAsPromised);
const expect = chai.expect

interface SimpleItem {
    id?: number;
    name: string;
}


describe("the use infinite page composable", () => {

    it("should have a blank initial state", (): void => {
        const mockApi: CrudApi = mock();
        const api = instance(mockApi);

        const { isUnloaded, isLoading, isLoaded, isFailed, hasItems, hasReachedLastPage, error, page, items } = useInfinitePage<SimpleItem>(api, "item");

        expect(isUnloaded.value).to.be.true;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.false;
        expect(hasItems.value).to.be.false;
        expect(hasReachedLastPage.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.be.undefined;
        expect(items.value).to.be.undefined;
    })

    it("should retrieve a collection of items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 1, undefined)).thenResolve(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: 2,
                    last: 2
                },
                items: [
                    { id: 1, name: "first" },
                    { id: 2, name: "second" },
                    { id: 3, name: "third" }
                ],
                meta: {}
            }
        )

        const { isUnloaded, isLoading, isLoaded, isFailed, hasItems, hasReachedLastPage, error, page, items, listMoreItems } = useInfinitePage<SimpleItem>(api, "item");

        const promise = listMoreItems();
        await expect(promise).to.eventually.eql(
            [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
        );

        verify(mockApi.page("item", 1, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(hasItems.value).to.be.true;
        expect(hasReachedLastPage.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 1,
                previous: null,
                next: 2,
                last: 2
            },
            items: [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])
    })

    it("should retrieve more items from the second page", async () => {

        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 1, undefined)).thenResolve(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: 2,
                    last: 2
                },
                items: [
                    { id: 1, name: "first" },
                    { id: 2, name: "second" },
                    { id: 3, name: "third" }
                ],
                meta: {}
            }
        )

        when(mockApi.page<SimpleItem>("item", 2, undefined)).thenResolve(
            {
                page: {
                    current: 2,
                    previous: 1,
                    next: null,
                    last: 2
                },
                items: [
                    { id: 4, name: "fourth" },
                    { id: 5, name: "fifth" },
                    { id: 6, name: "sixth" }
                ],
                meta: {}
            }
        )

        const { isUnloaded, isLoading, isLoaded, isFailed, hasItems, hasReachedLastPage, error, page, items, listMoreItems } = useInfinitePage<SimpleItem>(api, "item");

        const firstPagePromise = listMoreItems();
        await expect(firstPagePromise).to.eventually.eql(
            [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
        );

        verify(mockApi.page("item", 1, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(hasItems.value).to.be.true;
        expect(hasReachedLastPage.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 1,
                previous: null,
                next: 2,
                last: 2
            },
            items: [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])


        const secondPagePromise = listMoreItems();
        await expect(secondPagePromise).to.eventually.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" },
            { id: 4, name: "fourth" },
            { id: 5, name: "fifth" },
            { id: 6, name: "sixth" }
        ])

        verify(mockApi.page("item", 2, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(hasItems.value).to.be.true;
        expect(hasReachedLastPage.value).to.be.true;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 2,
                previous: 1,
                next: null,
                last: 2
            },
            items: [
                { id: 4, name: "fourth" },
                { id: 5, name: "fifth" },
                { id: 6, name: "sixth" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" },
            { id: 4, name: "fourth" },
            { id: 5, name: "fifth" },
            { id: 6, name: "sixth" }
        ])

    });

    it("should stop retrieving when the next page number is null", async () => {

        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 1, undefined)).thenResolve(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: null,
                    last: 1
                },
                items: [
                    { id: 1, name: "first" },
                    { id: 2, name: "second" },
                    { id: 3, name: "third" }
                ],
                meta: {}
            }
        )

        const { isUnloaded, isLoading, isLoaded, isFailed, hasItems, hasReachedLastPage, error, page, items, listMoreItems } = useInfinitePage<SimpleItem>(api, "item");

        const firstPagePromise = listMoreItems();
        await expect(firstPagePromise).to.eventually.eql(
            [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
        );

        verify(mockApi.page("item", 1, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(hasItems.value).to.be.true;
        expect(hasReachedLastPage.value).to.be.true;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 1,
                previous: null,
                next: null,
                last: 1
            },
            items: [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])


        const secondPagePromise = listMoreItems();
        await expect(secondPagePromise).to.eventually.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" },
        ])

        verify(mockApi.page("item", 2, undefined)).never()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(hasItems.value).to.be.true;
        expect(hasReachedLastPage.value).to.be.true;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 1,
                previous: null,
                next: null,
                last: 1
            },
            items: [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])
    })

    it("should retrieve a collection of filtered items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 1, deepEqual({ search: "th" }))).thenResolve(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: null,
                    last: 1
                },
                items: [
                    { id: 3, name: "third" },
                    { id: 4, name: "fourth" },
                    { id: 5, name: "fifth" },
                    { id: 6, name: "sixth" }
                ],
                meta: {}
            }
        )

        const { isUnloaded, isLoading, isLoaded, isFailed, hasItems, hasReachedLastPage, error, page, items, listMoreItems } = useInfinitePage<SimpleItem>(api, "item");

        const promise = listMoreItems({ search: "th" });
        await expect(promise).to.eventually.eql(
            [
                { id: 3, name: "third" },
                { id: 4, name: "fourth" },
                { id: 5, name: "fifth" },
                { id: 6, name: "sixth" }
            ]
        );

        verify(mockApi.page("item", 1, deepEqual({ search: "th" }))).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(hasItems.value).to.be.true;
        expect(hasReachedLastPage.value).to.be.true;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 1,
                previous: null,
                next: null,
                last: 1
            },
            items: [
                { id: 3, name: "third" },
                { id: 4, name: "fourth" },
                { id: 5, name: "fifth" },
                { id: 6, name: "sixth" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 3, name: "third" },
            { id: 4, name: "fourth" },
            { id: 5, name: "fifth" },
            { id: 6, name: "sixth" }
        ])
    })

    it("should fail to retrieve a collection of items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 1, undefined)).thenReject(new Error("Mock API Error"))

        const { isUnloaded, isLoading, isLoaded, isFailed, hasItems, hasReachedLastPage, error, page, items, listMoreItems } = useInfinitePage<SimpleItem>(api, "item");

        const promise = listMoreItems();
        await expect(promise).to.eventually.be.rejectedWith("Mock API Error").and.be.an.instanceOf(Error);

        verify(mockApi.page("item", 1, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.true;
        expect(hasItems.value).to.be.false;
        expect(hasReachedLastPage.value).to.be.false;
        expect((error.value as Error).message).to.equal("Mock API Error");
        expect(page.value).to.be.undefined;
        expect(items.value).to.be.undefined;
    })

    it("should supply an empty array if items is unassigned and a final page is manually assigned", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        const { page, listMoreItems } = useInfinitePage<SimpleItem>(api, "item");

        page.value = {
            page: {
                current: 0,
                next: null,
                previous: null,
                last: 0,
            },
            items: [],
            meta: {}
        }

        const promise = listMoreItems();
        await expect(promise).to.eventually.eql([]);
    })



    it("should reset after retrieving a collection of items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.page<SimpleItem>("item", 1, undefined)).thenResolve(
            {
                page: {
                    current: 1,
                    previous: null,
                    next: 2,
                    last: 2
                },
                items: [
                    { id: 1, name: "first" },
                    { id: 2, name: "second" },
                    { id: 3, name: "third" }
                ],
                meta: {}
            }
        )

        const { isUnloaded, isLoading, isLoaded, isFailed, hasItems, hasReachedLastPage, error, page, items, reset, listMoreItems } = useInfinitePage<SimpleItem>(api, "item");

        const promise = listMoreItems();
        await expect(promise).to.eventually.eql(
            [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
        );

        verify(mockApi.page("item", 1, undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(hasItems.value).to.be.true;
        expect(hasReachedLastPage.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.eql({
            page: {
                current: 1,
                previous: null,
                next: 2,
                last: 2
            },
            items: [
                { id: 1, name: "first" },
                { id: 2, name: "second" },
                { id: 3, name: "third" }
            ],
            meta: {}
        })
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])

        await reset();

        expect(isUnloaded.value).to.be.true;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.false;
        expect(hasItems.value).to.be.false;
        expect(hasReachedLastPage.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(page.value).to.be.undefined;
        expect(items.value).to.be.undefined;
    })

});
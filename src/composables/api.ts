import { ref, Ref } from "vue";
import { Api } from "../api/api";
import { ApiMiddlware, ApiOptions, CrudApi } from "../api/types";
import { ApiComposable } from "./types";

/**
 * Create a new CRUD API instance using the supplied options and middleware.
 * 
 * @param options The options to use when creating the new CRUD API instance.
 * @param middlewares The API Middleware layers to supply to the new CRUD API instance.
 * @returns A reference containing the new CRUD API instance.
 */
export const useApi = (options?: ApiOptions, middlewares?: Array<ApiMiddlware>): ApiComposable => {
    const api = ref<CrudApi>(new Api(options, middlewares))
    return {
        api,
    }
}


/**
 * A reference containing a CRUD API instance.
 * 
 * This value is used in conjunction with the `useGlobalApi` composition function,
 * which will either initialize or simply return this reference.
 * 
 * If just a single API is used in an application, some developers may find this
 * "Global API" mechanism convenient.
 */
let globalApi: Ref<CrudApi>;

/**
 * Create a new global CRUD API instance or retrieve the existing global CRUD
 * API instance.
 * 
 * The first call to `useGlobalApi` will initialize a new CRUD API instance with
 * the supplied options and middlewares and return the wrapping reference.
 * 
 * Any future calls to `useGlobalApi` will simply return the same wrapping reference,
 * and the options and middlewares values supplied to the call will be entirely ignored.
 * 
 * @param options The options to use when creating the new CRUD API instance.
 * @param middlewares The API Middleware layers to supply to the new CRUD API instance.
 * @returns The global reference containing the new or existing CRUD API instance.
 */
export const useGlobalApi = (options?: ApiOptions, middlewares?: Array<ApiMiddlware>): ApiComposable => {
    if (typeof globalApi === "undefined") {
        globalApi = ref<CrudApi>(new Api(options, middlewares));
    }
    return {
        api: globalApi
    };
}

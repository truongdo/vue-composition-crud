import { ref } from "vue";
import { CrudApi, QueryOptions } from "../api/types";
import { CollectionComposable } from "./types";

export const useCollection = <ItemType>(api: CrudApi, resource: string): CollectionComposable<ItemType> => {
    const isUnloaded = ref<boolean>(true);
    const isLoading = ref<boolean>(false);
    const isLoaded = ref<boolean>(false);
    const isFailed = ref<boolean>(false);
    const error = ref<Error | unknown>();
    const items = ref<Array<ItemType> | undefined>();

    const reset = async (): Promise<void> => {
        isUnloaded.value = true;
        isLoading.value = false;
        isFailed.value = false;
        error.value = undefined;
        isLoaded.value = false;
        items.value = undefined;
    }

    const listItems = async (queryOptions?: QueryOptions, inPlace = false): Promise<Array<ItemType>> => {
        isUnloaded.value = false
        if (!inPlace) {
            isLoaded.value = false
            items.value = undefined
        }
        isFailed.value = false
        error.value = undefined
        isLoading.value = true
        try {
            items.value = await api.list<ItemType>(resource, queryOptions);
            isLoaded.value = true
            isLoading.value = false
            return items.value
        } catch (e: Error | unknown) {
            error.value = e
            isFailed.value = true
            isLoading.value = false
            throw e
        }
    }

    return {
        isUnloaded,
        isLoading,
        isLoaded,
        isFailed,
        error,
        items,
        reset,
        listItems,
    }
};

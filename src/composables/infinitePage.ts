import { ref } from "vue";
import { CrudApi, Page, QueryOptions } from "../api/types";
import { InfinitePageComposable } from "./types";

export const useInfinitePage = <ItemType>(api: CrudApi, resource: string): InfinitePageComposable<ItemType> => {
    const isUnloaded = ref<boolean>(true);
    const isLoading = ref<boolean>(false);
    const isLoaded = ref<boolean>(false);
    const isFailed = ref<boolean>(false);
    const hasItems = ref<boolean>(false);
    const hasReachedLastPage = ref<boolean>(false);
    const error = ref<Error | unknown>();
    const page = ref<Page<ItemType> | undefined>();
    const items = ref<Array<ItemType> | undefined>();

    const reset = async (): Promise<void> => {
        isUnloaded.value = true;
        isLoading.value = false;
        isFailed.value = false;
        error.value = undefined;
        hasItems.value = false;
        isLoaded.value = false;
        items.value = undefined;
        page.value = undefined;
        hasReachedLastPage.value = false;
    }

    const listMoreItems = async (queryOptions?: QueryOptions): Promise<Array<ItemType>> => {

        // If the next page is `null` then there are no more pages to load
        if (page.value?.page?.next === null) {
            return items.value || [];
        }

        isUnloaded.value = false
        isLoaded.value = false
        isFailed.value = false
        error.value = undefined
        isLoading.value = true
        try {
            const pageNumber = page.value?.page?.next ?? 1
            page.value = await api.page<ItemType>(resource, pageNumber, queryOptions);
            items.value = [...(items.value || []), ...page.value.items]
            isLoaded.value = true
            isLoading.value = false
            hasItems.value = true
            hasReachedLastPage.value = (page.value.page.next === null);
            return items.value
        } catch (e: Error | unknown) {
            error.value = e
            isFailed.value = true
            isLoading.value = false
            throw e
        }
    }

    return {
        isUnloaded,
        isLoading,
        isLoaded,
        isFailed,
        hasItems,
        hasReachedLastPage,
        error,
        page,
        items,
        reset,
        listMoreItems,
    }
};

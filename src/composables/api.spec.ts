import * as chai from "chai";
import { useApi, useGlobalApi } from "./api";
import { Api } from "..";

const expect = chai.expect


describe("the use api composable", () => {

    it("should supply an API object reference", (): void => {
        const { api } = useApi();
        expect(api.value).to.be.instanceof(Api);
    })

    it("should supply the same API object reference for the global API", (): void => {
        const { api: first } = useGlobalApi();
        const { api: second } = useGlobalApi();
        const { api: third } = useGlobalApi();

        expect(first).to.equal(second);
        expect(first.value).to.equal(second.value);

        expect(first).to.equal(third);
        expect(first.value).to.equal(third.value);
    })


    it("should share global API objects but create separate independent API objects", (): void => {
        const { api: firstGlobal } = useGlobalApi();
        const { api: secondGlobal } = useGlobalApi();
        const { api: thirdGlobal } = useGlobalApi();

        const { api: firstIndependent } = useApi();
        const { api: secondIndependent } = useApi();

        expect(firstGlobal).to.not.equal(firstIndependent);
        expect(secondGlobal).to.not.equal(firstIndependent);
        expect(thirdGlobal).to.not.equal(firstIndependent);
        expect(firstGlobal.value).to.not.equal(firstIndependent.value);
        expect(secondGlobal.value).to.not.equal(firstIndependent.value);
        expect(thirdGlobal.value).to.not.equal(firstIndependent.value);

        expect(firstIndependent).to.not.equal(secondIndependent);
        expect(firstIndependent.value).to.not.equal(secondIndependent.value);
    })

})

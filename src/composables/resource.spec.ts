import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import { mock, instance, when, verify, anything } from "ts-mockito";
import { CrudApi } from "../api/types";
import { useResource } from "./resource";

chai.use(chaiAsPromised);
const expect = chai.expect

interface SimpleItem {
    id?: number;
    name: string;
}


describe("the use resource composable", () => {
    it("should have a blank initial state", (): void => {
        const mockApi: CrudApi = mock();
        const api = instance(mockApi);

        const { isUnloaded, isLoading, isLoaded, isFailed, error, item } = useResource<SimpleItem>(api, "item");

        expect(isUnloaded.value).to.be.true;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(item.value).to.be.undefined;
    })

    it("should create an item", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.create<SimpleItem>("item", anything())).thenResolve({ id: 426, name: "my-new-item" });

        const { isUnloaded, isLoading, isLoaded, isFailed, error, item, createItem } = useResource<SimpleItem>(api, "item");

        const promise = createItem({ name: "my-new-item" });
        await expect(promise).to.eventually.eql({ id: 426, name: "my-new-item" })

        verify(mockApi.create("item", anything())).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(item.value).to.eql({ id: 426, name: "my-new-item" });
    })

    it("should fail to create an item", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.create<SimpleItem>("item", anything())).thenReject(new Error("Mock API Error"));

        const { isUnloaded, isLoading, isLoaded, isFailed, error, item, createItem } = useResource<SimpleItem>(api, "item");

        const promise = createItem({ name: "my-new-item" });
        await expect(promise).to.eventually.be.rejectedWith("Mock API Error").and.be.an.instanceOf(Error);

        verify(mockApi.create("item", anything())).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.true;
        expect((error.value as Error).message).to.equal("Mock API Error");
        expect(item.value).to.be.undefined;
    })

    it("should retrieve an item", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.retrieve<SimpleItem>("item", 123)).thenResolve({ id: 123, name: "foobar" })

        const { isUnloaded, isLoading, isLoaded, isFailed, error, item, retrieveItem } = useResource<SimpleItem>(api, "item");

        const promise = retrieveItem(123);
        await expect(promise).to.eventually.eql({ id: 123, name: "foobar" })

        verify(mockApi.retrieve("item", 123)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(item.value).to.eql({ id: 123, name: "foobar" });
    })

    it("should fail to retrieve an item", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.retrieve<SimpleItem>("item", 123)).thenReject(new Error("Mock API Error"))

        const { isUnloaded, isLoading, isLoaded, isFailed, error, item, retrieveItem } = useResource<SimpleItem>(api, "item");

        const promise = retrieveItem(123);
        await expect(promise).to.eventually.be.rejectedWith("Mock API Error").and.be.an.instanceOf(Error);

        verify(mockApi.retrieve("item", 123)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.true;
        expect((error.value as Error).message).to.equal("Mock API Error");
        expect(item.value).to.be.undefined;
    })

    it("should update an item", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.update<SimpleItem>("item", 7142, anything())).thenResolve({ id: 7142, name: "my-updated-item" });

        const { isUnloaded, isLoading, isLoaded, isFailed, error, item, updateItem } = useResource<SimpleItem>(api, "item");

        const promise = updateItem(7142, { name: "my-updated-item" });
        await expect(promise).to.eventually.eql({ id: 7142, name: "my-updated-item" })

        verify(mockApi.update("item", 7142, anything())).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(item.value).to.eql({ id: 7142, name: "my-updated-item" });
    })

    it("should fail to update an item", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.update<SimpleItem>("item", 7142, anything())).thenReject(new Error("Mock API Error"));

        const { isUnloaded, isLoading, isLoaded, isFailed, error, item, updateItem } = useResource<SimpleItem>(api, "item");

        const promise = updateItem(7142, { name: "my-updated-item" });
        await expect(promise).to.eventually.be.rejectedWith("Mock API Error").and.be.an.instanceOf(Error);

        verify(mockApi.update("item", 7142, anything())).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.true;
        expect((error.value as Error).message).to.equal("Mock API Error");
        expect(item.value).to.be.undefined;
    })

    it("should delete an item", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.delete("item", 4242)).thenResolve()
        const { isUnloaded, isLoading, isLoaded, isFailed, error, item, deleteItem } = useResource<SimpleItem>(api, "item");

        const promise = deleteItem(4242);
        await expect(promise).to.eventually.be.fulfilled;

        verify(mockApi.delete("item", 4242)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(item.value).to.be.undefined;
    })

    it("should fail to delete an item", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.delete("item", 4242)).thenReject(new Error("Mock API Error"))

        const { isUnloaded, isLoading, isLoaded, isFailed, error, item, deleteItem } = useResource<SimpleItem>(api, "item");

        const promise = deleteItem(4242);
        await expect(promise).to.eventually.be.rejectedWith("Mock API Error").and.be.an.instanceOf(Error);

        verify(mockApi.delete("item", 4242)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.true;
        expect((error.value as Error).message).to.equal("Mock API Error");
        expect(item.value).to.be.undefined;
    })

});

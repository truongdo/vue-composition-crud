import { ref } from "vue";
import { CrudApi } from "../api/types";
import { ResourceComposable } from "./types";

export const useResource = <ItemType>(api: CrudApi, resource: string): ResourceComposable<ItemType> => {
    const isUnloaded = ref<boolean>(true);
    const isLoading = ref<boolean>(false);
    const isLoaded = ref<boolean>(false);
    const isFailed = ref<boolean>(false);
    const error = ref<Error | unknown>();
    const item = ref<ItemType | undefined>();

    const createItem = async (value: ItemType): Promise<ItemType> => {
        isUnloaded.value = false
        isLoaded.value = false
        item.value = undefined
        isFailed.value = false
        error.value = undefined
        isLoading.value = true
        try {
            item.value = await api.create<ItemType>(resource, value);
            isLoaded.value = true
            isLoading.value = false
            return item.value
        } catch (e: Error | unknown) {
            error.value = e
            isFailed.value = true
            isLoading.value = false
            throw e
        }
    }

    const retrieveItem = async (id: string | number): Promise<ItemType> => {
        isUnloaded.value = false
        isLoaded.value = false
        item.value = undefined
        isFailed.value = false
        error.value = undefined
        isLoading.value = true
        try {
            item.value = await api.retrieve<ItemType>(resource, id);
            isLoaded.value = true
            isLoading.value = false
            return item.value
        } catch (e: Error | unknown) {
            error.value = e
            isFailed.value = true
            isLoading.value = false
            throw e
        }
    }

    const updateItem = async (id: string | number, value: ItemType): Promise<ItemType> => {
        isUnloaded.value = false
        isLoaded.value = false
        item.value = undefined
        isFailed.value = false
        error.value = undefined
        isLoading.value = true
        try {
            item.value = await api.update<ItemType>(resource, id, value);
            isLoaded.value = true
            isLoading.value = false
            return item.value
        } catch (e: Error | unknown) {
            error.value = e
            isFailed.value = true
            isLoading.value = false
            throw e
        }
    }

    const deleteItem = async (id: string | number): Promise<void> => {
        isUnloaded.value = false
        isLoaded.value = false
        item.value = undefined
        isFailed.value = false
        error.value = undefined
        isLoading.value = true
        try {
            await api.delete(resource, id);
            item.value = undefined
            isLoaded.value = true
            isLoading.value = false
            return
        } catch (e: Error | unknown) {
            error.value = e
            isFailed.value = true
            isLoading.value = false
            throw e
        }
    }

    return {
        isUnloaded,
        isLoading,
        isLoaded,
        isFailed,
        error,
        item,
        createItem,
        retrieveItem,
        updateItem,
        deleteItem,
    }
};

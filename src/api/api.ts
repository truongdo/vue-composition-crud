import { AuthenticationApiError, GenericApiError } from "./error";
import { ApiMiddlware, ApiOptions, CrudApi, FetchMeta, FetchOptions, Page, QueryOptions, RequestOptions } from "./types";
import crossFetch from "cross-fetch";
import { Response } from "cross-fetch";
import * as EventEmitter from "events";

export class Api extends EventEmitter implements CrudApi {

    options: ApiOptions = {}
    defaultOptions: ApiOptions = {
        baseUrl: "/",
        fetch: crossFetch,
        fetchOptions: {
            credentials: "same-origin",
            cache: "default",
            mode: "cors",
            redirect: "follow",
            referrerPolicy: "no-referrer-when-downgrade",
        }
    }

    middlewares: Array<ApiMiddlware>

    constructor(options?: ApiOptions, middlewares: Array<ApiMiddlware> = []) {
        super();
        this.options = this.configure(options || {})
        this.middlewares = middlewares
    }

    configure(options: ApiOptions): ApiOptions {
        const fetchOptions = {
            ...this.defaultOptions.fetchOptions,
            ...this.options.fetchOptions ?? {},
            ...options.fetchOptions ?? {}
        }
        this.options = {
            ...this.defaultOptions,
            ...this.options,
            ...options,
            fetchOptions
        }
        return this.options
    }

    get baseUrl(): string {
        return this.options.baseUrl as string
    }

    get fetchMethod(): ((input: RequestInfo, init?: RequestInit) => Promise<Response>) {
        return this.options.fetch as ((input: RequestInfo, init?: RequestInit) => Promise<Response>)
    }

    async authenticate(type: string, payload?: { [key: string]: unknown }): Promise<boolean> {
        for (const middleware of this.middlewares) {
            const handled = await middleware.authenticate(this, type, payload);
            if (handled) {
                this.emit("authenticated");
                return true;
            }
        }
        throw new AuthenticationApiError(`There is no registered authentication handler for the authentication type '${type}'`);
    }

    async deauthenticate(): Promise<boolean> {
        const promises = this.middlewares.map(m => m.deauthenticate(this));
        let results = [];
        try {
            results = await Promise.all(promises);
        } catch (e) {
            console.error(e)
            throw new AuthenticationApiError(`One or more deauthentication handlers failed to run successfully`, e);
        }
        if (!results.some(result => result === true)) {
            throw new AuthenticationApiError(`No handlers indicated that they performed a deauthentication operation successfully`);
        }
        this.emit("deauthenticated");
        return true
    }

    async get<ItemType>(resource: string, queryOptions?: QueryOptions): Promise<ItemType> {
        const queryParameters = this._getQueryParameters(queryOptions);
        return await this.fetch<ItemType, void>("GET", resource, queryParameters);
    }

    async list<ItemType>(resource: string, queryOptions?: QueryOptions): Promise<Array<ItemType>> {
        const queryParameters = this._getQueryParameters(queryOptions);
        return await this.fetch<Array<ItemType>, void>("GET", resource, queryParameters);
    }

    async page<ItemType>(resource: string, page = 1, queryOptions?: QueryOptions): Promise<Page<ItemType>> {
        const queryParameters = this._getQueryParameters(queryOptions, { page: String(page) });
        return await this.fetch<Page<ItemType>, void>("GET", resource, queryParameters);
    }

    async create<ItemType>(resource: string, item: ItemType): Promise<ItemType> {
        return await this.fetch<ItemType, ItemType>("POST", resource, undefined, item);
    }

    async retrieve<ItemType>(resource: string, id: string | number): Promise<ItemType> {
        return await this.fetch<ItemType, void>("GET", `${resource}/${id}`);
    }

    async update<ItemType>(resource: string, id: string | number, item: ItemType): Promise<ItemType> {
        return await this.fetch<ItemType, ItemType>("PATCH", `${resource}/${id}`, undefined, item);
    }

    async delete(resource: string, id: string | number): Promise<void> {
        return await this.fetch<void, void>("DELETE", `${resource}/${id}`);
    }

    protected _getQueryParameters(queryOptions?: QueryOptions, additionalParameters?: { [key: string]: string }): URLSearchParams | undefined {
        if (!queryOptions && !additionalParameters) {
            return undefined;
        }
        const result: URLSearchParams = new URLSearchParams()
        if (queryOptions) {
            if (queryOptions.search) {
                result.set("search", queryOptions.search)
            }
            if (queryOptions.filter) {
                for (const filterKey in queryOptions.filter) {
                    result.set(filterKey, queryOptions.filter[filterKey])
                }
            }
        }
        if (additionalParameters) {
            for (const parameterKey in additionalParameters) {
                result.set(parameterKey, additionalParameters[parameterKey]);
            }
        }
        return result;
    }

    async fetch<ResponsePayloadType, RequestPayloadType>(method: string, path: string, queryParameters?: URLSearchParams, payload?: RequestPayloadType, meta?: FetchMeta): Promise<ResponsePayloadType> {

        const headers = {
            "accept": "application/json",
            "content-type": "application/json"
        }

        let options: RequestOptions = {
            method,
            uri: `${this.options.baseUrl}${path}`,
            queryParameters,
            headers,
            payload,
            credentials: (this.options.fetchOptions as FetchOptions).credentials,
            cache: (this.options.fetchOptions as FetchOptions).cache,
            mode: (this.options.fetchOptions as FetchOptions).mode,
            redirect: (this.options.fetchOptions as FetchOptions).redirect,
            referrerPolicy: (this.options.fetchOptions as FetchOptions).referrerPolicy,
            meta,
        }

        options = await this._beforeFetch(options);

        if (typeof this.options.fetch === "undefined") {
            throw new GenericApiError("A fetch provider is not defined");
        } else {
            options.credentials = options.credentials ?? (this.options.fetchOptions as FetchOptions).credentials
            options.cache = options.cache ?? (this.options.fetchOptions as FetchOptions).cache
            options.mode = options.mode ?? (this.options.fetchOptions as FetchOptions).mode
            options.redirect = options.redirect ?? (this.options.fetchOptions as FetchOptions).redirect
            options.referrerPolicy = options.referrerPolicy ?? (this.options.fetchOptions as FetchOptions).referrerPolicy

            const body = (typeof options.payload !== "undefined") ? JSON.stringify(options.payload) : undefined

            const requestOptions: RequestInit = {
                method: options.method,
                headers: options.headers,
                body,
                credentials: options.credentials,
                cache: options.cache,
                mode: options.mode,
                redirect: options.redirect,
                referrerPolicy: options.referrerPolicy
            }

            const queryString = (queryParameters)
                ? "?" + queryParameters.toString()
                : ""

            const uri = `${options.uri}${queryString}`
            const response = await this.options.fetch(uri, requestOptions);
            let result: ResponsePayloadType = await response.json() as ResponsePayloadType;
            result = await this._afterFetch<ResponsePayloadType>(options, response, result) as ResponsePayloadType;

            if (!response.ok) {
                switch (response.status) {
                    case 401:
                        this.emit("authentication-failed", { options: { ...options }, response, result });
                        throw new AuthenticationApiError(response.statusText, result);
                    default:
                        this.emit("request-failed", { options: { ...options }, response, result });
                        throw new GenericApiError(response.statusText, result);
                }
            }

            return result
        }
    }

    protected async _beforeFetch(options: RequestOptions): Promise<RequestOptions> {
        for (const middleware of this.middlewares) {
            options = await middleware.beforeFetch(this, options)
        }
        return options
    }

    protected async _afterFetch<ResponsePayloadType>(options: RequestOptions, response: Response, result: ResponsePayloadType | unknown): Promise<ResponsePayloadType | unknown> {
        for (const middleware of this.middlewares) {
            result = await middleware.afterFetch(this, options, response, result)
        }
        return result;
    }

}
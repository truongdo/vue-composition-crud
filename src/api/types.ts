/**
 * Type definitions for API and related interfaces.
 * @module
 */

import { Response } from "cross-fetch";
import { CookiesStatic } from "js-cookie";
import { PageMiddleware } from "./middleware/page";
import * as EventEmitter from "events";


export type GenericDict = {
    [key: string]: unknown;
}

export type StringDict = {
    [key: string]: string;
}

/**
 * Query Options
 * 
 * An object that can be used to supply additional parameters when performing
 * an API query, including search and filter options.
 */
export interface QueryOptions {
    /**
     * Free-text to search for.
     */
    search?: string;

    /**
     * Key value pairs to filter the queried result set.
     */
    filter?: StringDict;
}

/**
 * Request Options
 * 
 * An object containing options that will be used to perform an API request.
 */
export interface RequestOptions {

    /**
     * The HTTP method to use when performing the request.
     * Typically GET, POST, PUT, PATCH, or DELETE.
     */
    method: string;

    /**
     * The full request URI.
     */
    uri: string;

    /**
     * URL Query String Parameters.
     */
    queryParameters?: URLSearchParams;

    /**
     * Headers to supply when performing the request.
     */
    headers: {
        [key: string]: string;
    };

    /**
     * The request body to supply with the request, if any.
     */
    payload?: unknown;

    /**
     * The `credentials` parameter supplied to a fetch() call.
     * See [the fetch api documentation](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#sending_a_request_with_credentials_included)
     * for full details.
     */
    credentials?: RequestCredentials;

    /**
     * The `cache` parameter supplied to a fetch() call.
     * See [the fetch api documentation](https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters) for full details.
     */
    cache?: RequestCache;

    /**
     * The `mode` parameter supplied to a fetch() call.
     * See [the fetch api documentation](https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters) for full details.
     */
    mode?: RequestMode;

    /**
     * The `redirect` parameter supplied to a fetch() call.
     * See [the fetch api documentation](https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters) for full details.
     */
    redirect?: RequestRedirect;

    /**
     * The `referrerPolicy` parameter supplied to a fetch() call.
     * See [the fetch api documentation](https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters) for full details.
     */
    referrerPolicy?: ReferrerPolicy;

    /**
     * Metadata.
     * This can be supplied when making the fetch() call, and can be accessed by middleware.
     */
    meta?: FetchMeta;
}

/**
 * Page
 * 
 * A paged resultset which includes the requested items,
 * as well as details about the page, and possibly extra
 * metadata.
 */
export interface Page<ItemType> {
    /**
     * Page details for this resultset.
     */
    page: {
        /**
         * The previous page number, if one is available.
         */
        previous?: number;
        /**
         * The current page number.
         */
        current: number;
        /**
         * The next page number, if one is available.
         */
        next?: number;
        /**
         * The last (final) page number, if one is available.
         */
        last?: number;
    };
    /**
     * The result set of the requested items as an array.
     */
    items: Array<ItemType>;
    /**
     * Metadata to augment this request, if any is supplied.
     */
    meta?: unknown;
}

/**
 * API Error
 * 
 * An error interface which defines some fields that occur
 * in error objects used throughout the API.
 */
export interface ApiError {
    /**
     * The message for this error.
     */
    message?: unknown;

    /**
     * Data supplied with the error, if any is available.
     * This can contain extra details about the error including
     * error messages, validation lists, and so on.
     */
    data?: unknown;

    /**
     * Metadata supplied by the code that generated the error,
     * if any is available and supplied. This may include further
     * details around the reason the error was produced, or may
     * contain contextual information which may help determine
     * the reason of the failure.
     */
    meta: GenericDict;
}

export interface FetchMeta extends GenericDict {
    /**
     * If set to true, then the request that is being performed is
     * part of an authentication operation. This may affect the behaviour
     * of middleware, and in particular, middleware responsible for
     * for performing authentication.
     */
    authenticating?: boolean;

    /**
     * If set to true, then tue request that is being performed is
     * part of an operation to retrieve a CSRF token. This may affect
     * the behaviour of middleware, and in particular, middleware
     * responsible for retrieving CRSF tokens.
     */
    gettingCsrfToken?: boolean;
}

export interface CrudApiEvents {
    authenticated(): void;
}

/**
 * CRUD API
 * 
 * The basic CRUD API interface, defines the main features and functions of
 * an API class within this package.
 */
export interface CrudApi {

    /**
     * An event which is raised when the authentication process completes successfully
     * @param event the authenticated event
     * @param listener the listener function that will be invoked
     */
    on(event: "authenticated", listener: () => void): this;

    /**
     * An event which is raised when a request fails with a 401 authentication error
     * @param event the authentication failed event
     * @param listener the listener function that will be invoked
     */
    on(event: "authentication-failed", listener: (data: { options: RequestOptions, response: ResponseInit, result: unknown }) => void): this;

    /**
     * An event which is raised when the deauthentication process completes successfully
     * @param event the deauthenticated event
     * @param listener the listener function that will be invoked
     */
    on(event: "deauthenticated", listener: () => void): this;

    /**
     * An event which is raised when a request fails with a non-authentication error
     * @param event the authentication failed event
     * @param listener the listener function that will be invoked
     */
    on(event: "request-failed", listener: (data: { options: RequestOptions, response: ResponseInit, result: unknown }) => void): this;

    /**
     * The Base URL upon which all request URLs are prefixed with.
     */
    baseUrl: string;

    /**
     * The fetch function used to perform web requests.
     * This can be the global fetch, an instance supplied by something
     * like whatwg or node-fetch, or a mocked fetch instance which can
     * be used to perform tests.
     */
    fetchMethod: ((input: RequestInfo, init?: RequestInit) => Promise<Response>);

    /**
     * Supply authentication details to the API.
     * 
     * By default, this method will throw an {@link AuthenticationApiError}.
     * Authentication providers are currently supplied via middleware.
     * 
     * Each authentication provider will supply its own type or types,
     * and each authentication type may require a payload to be supplied.
     * 
     * @param type The type of authentication details to supply.
     * @param payload The authentication details to supply.
     * @returns true if the authentication request was handled
     * by a middleware authentication provider.
     */
    authenticate(type: string, payload?: GenericDict): Promise<boolean>;

    /**
     * Retrieve an item.
     * 
     * This method performs a basic GET request and makes no assumption about
     * the type of data that is supplied by the target endpoint (i.e., whether
     * it is a resource, a page, a collection, or something else).
     * 
     * @param resource The name of the resource to get.
     * @param queryOptions Any query options to supply when retrieving the
     * requested resource.
     * @returns The fetched object of the requested type.
     * @throws {@link AuthenticationApiError} An Authentication API Error if the
     * request failed because the API is not authenticated.
     * @throws {@link GenericApiError} An API error if the request failed.
     */
    get<ItemType>(resource: string, queryOptions?: QueryOptions): Promise<ItemType>;

    /**
     * Retrieve a collection of items.
     * 
     * This method performs a basic GET request and assumes that the resulting
     * payload will be an array of objects of the supplied ItemType.
     * 
     * @param resource The name of the list resource to retrieve.
     * @param queryOptions Any query options to supply when retrieving the
     * requested resource, for example search and filter.
     * @returns The fetched collection of the requested type.
     * @throws {@link AuthenticationApiError} An Authentication API Error if the
     * request failed because the API is not authenticated.
     * @throws {@link GenericApiError} An API error if the request failed.
     */
    list<ItemType>(resource: string, queryOptions?: QueryOptions): Promise<Array<ItemType>>;

    /**
     * Retrieve a paged collection of items.
     * 
     * This method performs a basic GET request and assumes that the resulting
     * payload will be a paged collection of objects of the supplied ItemType.
     * The paged collection includes details about curent, previous, and next
     * pages, the retrieved collection of objects, and maybe some metadata.
     * 
     * @param resource The name of the paged resource to retrieve.
     * @param queryOptions Any query options to supply when retrieving the
     * requested resource, for example search and filter.
     * @returns The fetched page of the requested type.
     * @throws {@link AuthenticationApiError} An Authentication API Error if the
     * request failed because the API is not authenticated.
     * @throws {@link GenericApiError} An API error if the request failed.
     */
    page<ItemType>(resource: string, page: number, queryOptions?: QueryOptions): Promise<Page<ItemType>>;

    /**
     * Create a new resource item.
     * 
     * This method performs a basic POST request to create a new resource
     * of the specified type.
     * 
     * This method assumes that the ItemType is supplied to create the item,
     * and is also returned by the remote server when the item is created.
     * If this is not the case, you may wish to use a union type for this
     * method (e.g.: `create(<CreateItemType|ResponseItemType>(...))`)
     * 
     * @param resource The name of the resource type to create.
     * @param item The object details used to create the new resource item.
     * @returns The newly created resource item suppled upon successful creation.
     * @throws {@link AuthenticationApiError} An Authentication API Error if the
     * request failed because the API is not authenticated.
     * @throws {@link GenericApiError} An API error if the request failed.
     */
    create<ItemType>(resource: string, item: ItemType): Promise<ItemType>;


    /**
     * Retrieve an existing resource item.
     * 
     * This method performs a basic GET request to retrieve an existing
     * resource of the specified type.
     * 
     * @param resource The name of the resource type to retrieve.
     * @param id The id of the specific resource item to retrieve.
     * @returns The fetched resource item of the requested type.
     * @throws {@link AuthenticationApiError} An Authentication API Error if the
     * request failed because the API is not authenticated.
     * @throws {@link GenericApiError} An API error if the request failed.
     */
    retrieve<ItemType>(resource: string, id: string | number): Promise<ItemType>;

    /**
     * Update an existing resource item.
     * 
     * This method performs a basic PATCH request to update an existing
     * resource of the specified type.
     * 
     * This method assumes that the ItemType is supplied to create the item,
     * and is also returned by the remote server when the item is created.
     * If this is not the case, you may wish to use a union type for this
     * method (e.g.: `create(<UpdateItemType|ResponseItemType>(...))`)
     * 
     * @param resource The name of the resource type to update.
     * @param id The id of the specific resource item to update.
     * @param item The object details used to update the existing resource item.
     * @returns The updated resource item suppled upon successful update.
     * @throws {@link AuthenticationApiError} An Authentication API Error if the
     * request failed because the API is not authenticated.
     * @throws {@link GenericApiError} An API error if the request failed.
     */
    update<ItemType>(resource: string, id: string | number, item: ItemType): Promise<ItemType>;

    /**
     * Delete an existing resource item.
     * 
     * This method performs a basic DELETE request to delete an existing
     * resource of the specified type.
     * 
     * @param resource The name of the resource type to delete.
     * @param id The id of the specific resource item to delete.
     * @throws {@link AuthenticationApiError} An Authentication API Error if the
     * request failed because the API is not authenticated.
     * @throws {@link GenericApiError} An API error if the request failed.
     */
    delete(resource: string, id: string | number): Promise<void>;

    /**
     * Perform a fetch request.
     * This method involves all standard processing including middleware.
     * 
     * @param method The request method (usually GET, POST, PUT, PATCH, DELETE, ...)
     * @param path The path to perform the request to.
     * @param queryParameters Any URL query parameters to supply.
     * @param payload The request payload which, if supplied, will be JSON encoded.
     * @returns The requested response payload.
     * @throws {@link AuthenticationApiError} An Authentication API Error if the
     * request failed because the API is not authenticated.
     * @throws {@link GenericApiError} An API error if the request failed.
     */
    fetch<ResponsePayloadType, RequestPayloadType>(method: string, path: string, queryParameters?: URLSearchParams, payload?: RequestPayloadType, meta?: FetchMeta): Promise<ResponsePayloadType>;
}

export interface FetchOptions {
    credentials?: RequestCredentials;
    cache?: RequestCache;
    mode?: RequestMode;
    redirect?: RequestRedirect;
    referrerPolicy?: ReferrerPolicy;
}

/**
 * Configuration Options for a {@link CrudApi} implementation
 */
export interface ApiOptions {
    /**
     * The Base URL of the API.
     * This URL is prefixed to every request.
     */
    baseUrl?: string

    /**
     * The fetch function to use when performing fetch() requests.
     */
    fetch?(input: RequestInfo, init?: RequestInit): Promise<Response>

    /**
     * Default options supplied to a fetch request
     */
    fetchOptions?: FetchOptions;

}

/**
 * API Middleware
 * 
 * This interface defines the methods that an API Middleware implementation
 * should supply. These methods are used by the {@link CrudApi} to perform
 * authentication, pre-fetch, and post-fetch operations, and are the hooks
 * that allow the middleware to perform its job.
 */
export interface ApiMiddlware {
    /**
     * Perform an authentication operation.
     * 
     * This hook is invoked when the authenticate method of the {@link CrudApi}
     * implementation is invoked.
     * 
     * If this middleware does not support authentication operations, then
     * this function must return `false`.
     * 
     * If this middleware supports the requested authentication type, then
     * it should perform the necessary tasks and then return `true`.
     * 
     * @param api The {@link CrudApi} instance performing the authentication request.
     * @param type The type of authentication to perform. This authentication type
     * is free-form and is defined by each {@link ApiMiddleware} implementation to
     * suit its offerings.
     * @param payload The authentication payload which contains details necessary
     * to perform authentication. For example, a "login" style authentication
     * middleware may require a payload containing a username and password, while
     * an API Key authentication middleware may require a payload containing a key.
     * @returns `true` if this middleware supports and has handled the requested
     * authentication operation, or `false` otherwise.
     */
    authenticate(api: CrudApi, type: string, payload?: GenericDict): Promise<boolean>;

    /**
     * Performs a deauthentication operation.
     * 
     * This hook is invoked when the deauthenticate method of the {@link CrudApi}
     * implementation is invoked.
     * 
     * If this middleware does not support authentication operations, then this
     * function must return `false`.
     * 
     * If this middleware supports any authentication operation, and authentication has
     * happened with this middleware, then this method must perform any required
     * operations to deauthenticate the user, and then return `true`.
     * 
     * @param api The {@link CrudApi} instance performing the authentication request.
     * @returns `true` if this middleware supports and has handled the requested
     * authentication operation, or `false` otherwise.
     * @throws an error if deauthentication could not be successfully completed.
     */
    deauthenticate(api: CrudApi): Promise<boolean>;

    /**
     * Perform a pre-request hook.
     * 
     * This hook is invoked just prior to the {@link CrudApi} performing an
     * API query.
     * 
     * This hook provides an opportunity for the middleware to perform any
     * transformations that may be required to the API request options.
     * For example, the middleware may add or modify request headers.
     * 
     * By default, this hook returns the options parameter that were supplied.
     * 
     * @param api The {@link CrudApi} instance performing the fetch request.
     * @param options The current set of options which will be used to
     * perform the API request. These options may have been modified by other
     * middleware layers prior to reaching this hook for the current middleware.
     * @returns The new set of options which will be used to perform the API
     * request.
     */
    beforeFetch(api: CrudApi, options: RequestOptions): Promise<RequestOptions>;

    /**
     * Perform a post-request hook.
     * 
     * This hook is invoked after the {@link CrudApi} has performed an
     * API query.
     * 
     * This hook provides an opportunity for the middleware to perform any
     * transformations to the response payload that may be required.
     * 
     * By default, this hook returns the result parameter that was supplied.
     * 
     * @param api The {@link CrudApi} instance that performed the fetch request.
     * @param options The options that were used to perform the request.
     * @param response The response object that was received.
     * @param result The result payload which will be supplied as a result
     * of the fetch operation. This generally starts off as the response payload
     * received from the request, however this object may have been modified by
     * other middleware layers prior to reaching this hook for the current middleware.
     * @returns The new result object which will be supplied as the result for
     * the API query.
     */
    afterFetch<ResponsePayloadType>(api: CrudApi, options: RequestOptions, response: Response, result: ResponsePayloadType | unknown): Promise<ResponsePayloadType | unknown>;
}

/**
 * Token Authentication Middleware Options
 * 
 * Configuration Options for a {@link ApiMiddleware} implementation.
 */
export interface TokenAuthenticationMiddlewareOptions {
    /**
     * The path used to perform a password-based login.
     */
    passwordLoginPath?: string;

    /**
     * The key of the response from a password-based login which
     * contains the access token.
     */
    passwordLoginAccessTokenKey?: string;

    /**
     * The key of the response from a password-based login which
     * contains the refresh token.
     */
    passwordLoginRefreshTokenKey?: string;

    /**
     * The key of the request body sent for a password-based login
     * which will contain the username.
     */
    passwordLoginUsernameKey?: string;

    /**
     * The key of the request body sent for a password-based login
     * which will contain the password.
     */
    passwordLoginPasswordKey?: string;

    /**
     * The path used to perform a token refresh.
     */
    refreshTokenPath?: string;

    /**
     * The key of the request body sent for a token refresh
     * which will contain the refresh token.
     */
    refreshTokenRefreshTokenKey?: string;

    /**
     * The key of the response from a token refresh which
     * contains the refreshed access token.
     */
    refreshTokenAccessTokenKey?: string;

    /**
     * The key of the response from a token refresh which
     * contains the expiry time of the new token.
     */
    refreshTokenExpiresAtKey?: string

    /**
     * Buffer time (in seconds) which will cause the middleware
     * to refresh an access token before it has expired, if
     * it is close to expiry but not quite expired.
     * 
     * For example, if the token will expire in 5 seconds but
     * the refresh buffer time is set to 10 seconds, then the
     * token will be refreshed at this point because it is
     * so close to expiring that it may have already expired
     * by the time the request is made.
     */
    refreshBufferSeconds?: number;
}

export interface SessionAuthenticationMiddlewareOptions {
    /**
     * The path used to perform a password-based login.
     */
    passwordLoginPath?: string;

    /**
     * The key of the request body sent for a password-based login
     * which will contain the username.
     */
    passwordLoginUsernameKey?: string;

    /**
     * The key of the request body sent for a password-based login
     * which will contain the password.
     */
    passwordLoginPasswordKey?: string;

    /**
     * The path used to perform a logout for a password-based login.
     */
    passwordLogoutPath?: string;
}

export type ArrowFunction<T> = () => T;

export interface BearerTokenAuthenticationMiddlewareOptions {
    /**
     * The token, or a function which resolves the token.
     */
    token?: string | ArrowFunction<string>;

    /**
     * The header name used when supplying the token.
     */
    headerName?: string;

    /**
     * The header value prefix prepended to the token.
     */
    headerPrefix?: string;
}

export interface PageMiddlewareOptions {

    /**
     * Specify the mode of operation.
     * "off": the page middleware is not used.
     * "page": the page middleware operates in page mode.
     * "offset": the page middleware operates in offset mode.
     */
    mode?: "off" | "page" | "offset";

    /**
     * If true, then the page size will be supplied as
     * a query parameter for a usePage request.
     * This only applies to "page" mode.
     * The page size will always be supplied in "offset" mode.
     */
    usePageSize?: boolean;

    /**
     * If supplied, then a page size will be included
     */
    pageSize?: number;

    /**
     * The parameter name of the query parameter supplied
     * to request a specific page number.
     */
    pageParameterName?: string;

    /**
     * The parameter name of the query parameter supplied
     * to request an offset resultset.
     */
    offsetParameterName?: string;

    /**
     * The parameter name of the query parameter supplied
     * to request a page of a specific size.
     */
    pageSizeParameterName?: string;

    /**
     * Map a response object from the format sent by the
     * API endpoint, to a Page object usable with this API.
     * 
     * This can be used to perform simple (or complex) transformations
     * to the response object, for example moving or renaming keys.
     * 
     * This could be use to transform something like:
     *  `{page: 1, data: [...]}`
     * 
     * into:
     *  `{pages: {current: 1, previous: null, next: null}, items: [...]}`
     */
    mapResult?: ((data: unknown, pageMiddleware: PageMiddleware, api: CrudApi) => Promise<Page<unknown>>);

}

export interface TrailingSlashMiddlewareOptions {

    /**
     * Specify whether this trailing slash middleware ensures that
     * a trailing slash is always present, or is always absent.
     */
    mode?: "absent" | "present"

}

export interface CsrfTokenMiddlewareOptions {

    /**
     * Specify the cookie provider which is used to
     * interact with cookies.
     */
    cookieProvider?: CookiesStatic;

    /**
     * Specify how the CSRF token should be received.
     */
    receiveMode?: "off" | "header" | "cookie" | "function";

    /**
     * If the CSRF token is provided by a response header,
     * specify the name of the header to read.
     */
    receiveCsrfHeaderName?: string;

    /**
     * If the CSRF token is provided by a response header,
     * specify the URL of the path to perform a GET request
     * to to retrieve the initial CSRF token.
     */
    receiveCsrfHeaderPath?: string;

    /**
     * If the CSRF token is provided in a cookie, specify the
     * name of the cookie to read.
     */
    receiveCsrfCookieName?: string;

    /**
     * If the CSRF token is provided by a cookie,
     * specify the URL of the path to perform a GET request
     * to to retrieve the initial CSRF token.
     */
    receiveCsrfCookiePath?: string;

    /**
     * If the CSRF token is supplied by a custom method, supply
     * the function used to retrieve the CSRF token.
     */
    receiveCsrfFunction?: () => Promise<string | undefined>;

    /**
     * Specify whether the CSRF token should be supplied as a header
     * or injected into the body of the request.
     */
    sendMode?: "off" | "header" | "body";

    /**
     * If the CSRF token is injected as part of the body payload,
     * specify the field name to use.
     */
    sendCsrfFieldName?: string;

    /**
     * If the CSRF token is injected as a HTTP header, specify
     * the field name to use.
     */
    sendCsrfHeaderName?: string;

    /**
     * A list of HTTP methods for which the CSRF token should be sent.
     */
    sendMethodAllowlist?: Array<string>;

    /**
     * A list of HTTP methods for which the CSRF token should not be sent.
     */
    sendMethodBlocklist?: Array<string>;

}
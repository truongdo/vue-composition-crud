import { ApiError } from "./types";

/**
 * Base API Error.
 * All API errors should extend this class.
 */
export class BaseApiError extends Error implements ApiError {

    data: unknown
    meta: { [key: string]: unknown }

    constructor(message: string, data?: unknown, meta?: { [key: string]: unknown }) {
        super(message);
        this.data = data
        this.meta = meta || {}
    }

}

/**
 * Generic API Error.
 * An API error that does not fit into any specific API Error category.
 */
export class GenericApiError extends BaseApiError {}

/**
 * Authentication API Error.
 * An API error caused by bad/expired/unsupplied credentials.
 */
export class AuthenticationApiError extends BaseApiError {}

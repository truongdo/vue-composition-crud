import { BaseApiError, GenericApiError, AuthenticationApiError } from "./error"
import { expect } from "chai"

describe("API Errors", () => {
    it("should all implemenet the BaseApiError interface", () => {
        const plainBaseApiError = new BaseApiError("message only");
        const baseApiError = new BaseApiError("base message", "base data", { "meta key 0": "meta value 0" });
        const genericApiError = new GenericApiError("generic message", "generic data", { "meta key 1": "meta value 1" });
        const authenticationApiError = new AuthenticationApiError("authentication message", "authentication data", { "meta key 2": "meta value 2" })

        expect(plainBaseApiError).to.be.instanceOf(BaseApiError);
        expect(plainBaseApiError.message).to.equal("message only");
        expect(plainBaseApiError.data).to.be.undefined;
        expect(plainBaseApiError.meta).to.eql({});

        expect(baseApiError).to.be.instanceOf(BaseApiError);
        expect(baseApiError.message).to.equal("base message");
        expect(baseApiError.data).to.equal("base data");
        expect(baseApiError.meta).to.have.key("meta key 0");
        expect(baseApiError.meta["meta key 0"]).to.equal("meta value 0");

        expect(genericApiError).to.be.instanceOf(GenericApiError);
        expect(genericApiError).to.be.instanceOf(BaseApiError);
        expect(genericApiError.message).to.equal("generic message");
        expect(genericApiError.data).to.equal("generic data");
        expect(genericApiError.meta).to.have.key("meta key 1");
        expect(genericApiError.meta["meta key 1"]).to.equal("meta value 1");

        expect(authenticationApiError).to.be.instanceOf(AuthenticationApiError);
        expect(authenticationApiError).to.be.instanceOf(BaseApiError);
        expect(authenticationApiError.message).to.equal("authentication message");
        expect(authenticationApiError.data).to.equal("authentication data");
        expect(authenticationApiError.meta).to.have.key("meta key 2");
        expect(authenticationApiError.meta["meta key 2"]).to.equal("meta value 2");
    })
})
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as chaiFetchMock from "chai-fetch-mock";
import * as fetchMock from "fetch-mock";
import { deepEqual } from "ts-mockito";
import { Api, AuthenticationApiError } from "../..";
import { SessionAuthenticationMiddleware } from "./sessionAuthentication"

chai.use(chaiAsPromised);
chai.use(chaiFetchMock);
const expect = chai.expect;

const fetch = fetchMock.sandbox();


describe("Session Authentication Middleware", () => {

    describe("Authentication Attempts", () => {


        it("authenticates with a username and password", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "abcd", refresh: "efgh" }))

            const middleware = new SessionAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "password", { "username": "jimbo", "password": "goat" });

            expect(handled).to.be.true;

            fetch.restore();
        })

        it("fails to authenticate with a username and password", async () => {
            fetch.post(
                "https://www.example.com/login",
                {
                    status: 400,
                    body: { detail: "The username or password is incorrect" }
                }
            )

            const middleware = new SessionAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const promise = middleware.authenticate(api, "password", { "username": "jimbo", "password": "goat" });
            await expect(promise).to.eventually.be.rejectedWith("Authentication attempt failed: Bad Request")
                .and.be.an.instanceOf(AuthenticationApiError)
                .and.have.property("data")
                .to.have.property("detail", "The username or password is incorrect");

            fetch.restore();
        })

        it("fails to authenticate with no payload", async () => {
            fetch.post(
                "https://www.example.com/login",
                {
                    status: 400,
                    body: { detail: "The username and password were not supplied" }
                }
            )

            const middleware = new SessionAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const promise = middleware.authenticate(api, "password", undefined);
            await expect(promise).to.eventually.be.rejectedWith("Authentication attempt failed: Bad Request")
                .and.be.an.instanceOf(AuthenticationApiError)
                .and.have.property("data")
                .to.have.property("detail", "The username and password were not supplied");

            fetch.restore();
        })

        it("fails to authenticate with no username or password", async () => {
            fetch.post(
                "https://www.example.com/login",
                {
                    status: 400,
                    body: { detail: "The username and password were not supplied" }
                }
            )

            const middleware = new SessionAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const promise = middleware.authenticate(api, "password", {});
            await expect(promise).to.eventually.be.rejectedWith("Authentication attempt failed: Bad Request")
                .and.be.an.instanceOf(AuthenticationApiError)
                .and.have.property("data")
                .to.have.property("detail", "The username and password were not supplied");

            fetch.restore();
        })

        it("does not handle unknown authentication types", async () => {
            const middleware = new SessionAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "unknown", {});

            expect(handled).to.be.false;
        })

    })



    describe("Deuthentication Attempts", () => {

        it("deauthenticates with a username and password", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "abcd", refresh: "efgh" }))
            fetch.post("https://www.example.com/logout", JSON.stringify({ status: "ok" }))

            const middleware = new SessionAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handledAuth = await middleware.authenticate(api, "password", { "username": "jimbo", "password": "goat" });
            expect(handledAuth).to.be.true;
            expect(fetch.called("https://www.example.com/login")).to.be.true;

            const handledDeauth = await middleware.deauthenticate(api);
            expect(handledDeauth).to.be.true;
            expect(fetch.called("https://www.example.com/logout")).to.be.true;

            fetch.restore();
        })

        it("deauthenticates without prior authentication", async () => {
            fetch.post("https://www.example.com/logout", JSON.stringify({ status: "ok" }))

            const middleware = new SessionAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handledDeauth = await middleware.deauthenticate(api);
            expect(handledDeauth).to.be.true;
            expect(fetch.called("https://www.example.com/logout")).to.be.true;

            fetch.restore();
        })


        it("does not deauthenticate on deauthentication failure", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "abcd", refresh: "efgh" }))
            fetch.post("https://www.example.com/logout", {
                status: 400,
                body: { detail: "Logout failed because reasons" }
            })

            const middleware = new SessionAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handledAuth = await middleware.authenticate(api, "password", { "username": "jimbo", "password": "goat" });
            expect(handledAuth).to.be.true;
            expect(fetch.called("https://www.example.com/login")).to.be.true;

            const promise = middleware.deauthenticate(api);
            await expect(promise).to.eventually.be.rejectedWith("Deauthentication attempt failed: Bad Request")
                .and.be.an.instanceOf(AuthenticationApiError)
                .and.have.property("data")
                .to.have.property("detail", "Logout failed because reasons");
            expect(fetch.called("https://www.example.com/logout")).to.be.true;

            fetch.restore();
        })

    })

})
import { CrudApi, RequestOptions, TrailingSlashMiddlewareOptions } from "../types";
import { BaseMiddleware } from "./base";



/**
 * Trailing Slash Middleware.
 * 
 * Ensures that trailing slash rules are applied to all URIs.
 * By default, it ensures that all URIs end with a trailing slash,
 * but it also can be configured to remove all trailing slashes instead.
 */
export class TrailingSlashMiddleware extends BaseMiddleware {

    options: TrailingSlashMiddlewareOptions = {}

    defaultOptions: TrailingSlashMiddlewareOptions = {
        mode: "present"
    }


    /**
     * Create a new Trailing Slash Middleware instance with
     * the supplied options.
     * 
     * @param options The options to configure the middleware with.
     */
    constructor(options?: TrailingSlashMiddlewareOptions) {
        super()
        this.options = this.configure(options || {})
    }

    /**
     * Update the configuration of this middleware with the
     * supplied options.
     * 
     * @param options The options to update for this middleware.
     * @returns The new configuration options for this middleware.
     */
    public configure(options: TrailingSlashMiddlewareOptions): TrailingSlashMiddlewareOptions {
        this.options = {
            ...this.defaultOptions,
            ...this.options,
            ...options
        }
        return this.options
    }

    async beforeFetch(api: CrudApi, options: RequestOptions): Promise<RequestOptions> {
        switch (this.options.mode) {
            case "present": {
                if (!options.uri.endsWith("/")) {
                    options.uri = `${options.uri}/`
                }
                break;
            }
            case "absent": {
                if (options.uri.endsWith("/")) {
                    options.uri = options.uri.replace(/\/*$/, "");
                }
                break;
            }
            default: {
                console.warn(`The Trailing Slash Middleware mode '${this.options.mode}' is invalid`)
            }
        }
        return options
    }

}
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as chaiSpies from "chai-spies";
import { mock, instance } from "ts-mockito";
import { PageMiddleware } from "./page"
import { CrudApi, Page } from "../types";

chai.use(chaiAsPromised);
chai.use(chaiSpies);
const expect = chai.expect;



describe("Page Middleware", () => {

    describe("Constructor", () => {
        it("initializes with default options", async () => {
            const middleware = new PageMiddleware();
            expect(middleware.options).to.eql({
                mode: "page",
                pageSize: 20,
                pageParameterName: "page",
                offsetParameterName: "offset",
                pageSizeParameterName: "count",
                mapResult: undefined,
            })
        })
    })

    describe("Inactive Request", () => {

        it("Passes through a regular page when all modes are disabled", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middlware = new PageMiddleware({ mode: "off" })
            const requestOptions = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ page: "3" }),
                body: undefined,
            }

            const newRequestOptions = await middlware.beforeFetch(api, requestOptions);

            expect(newRequestOptions).to.eql({
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ page: "3" }),
                body: undefined,
            })

        })



        it("does not map responses when all modes are disabled", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            const middlware = new PageMiddleware({ mode: "off", pageParameterName: "mypage" })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ mypage: "3" }),
                body: undefined,
            }
            const result = {
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            })

        })


        it("does not map responses when an unknown mode is supplied", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            const middlware = new PageMiddleware({ mode: "some-unknown-unused-mode-stub", pageParameterName: "mypage" })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ mypage: "3" }),
                body: undefined,
            }
            const result = {
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            })

        })

    })

    describe("Page Mode", () => {

        it("does nothing when no page is supplied", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middlware = new PageMiddleware({ mode: "page", pageParameterName: "mypage" })
            const requestOptions = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ foo: "bar", baz: "bop" }),
                body: undefined,
            }

            const newRequestOptions = await middlware.beforeFetch(api, requestOptions);

            expect(newRequestOptions).to.eql({
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ foo: "bar", baz: "bop" }),
                body: undefined,
            })

        })


        it("does nothing query parameters are undefined", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middlware = new PageMiddleware({ mode: "page", pageParameterName: "mypage" })
            const requestOptions = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: undefined,
                body: undefined,
            }

            const newRequestOptions = await middlware.beforeFetch(api, requestOptions);

            expect(newRequestOptions).to.eql({
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: undefined,
                body: undefined,
            })

        })

        it("transforms the page query name", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middlware = new PageMiddleware({ mode: "page", pageParameterName: "mypage" })
            const requestOptions = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ page: "3" }),
                body: undefined,
            }

            const newRequestOptions = await middlware.beforeFetch(api, requestOptions);

            expect(newRequestOptions).to.eql({
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ mypage: "3" }),
                body: undefined,
            })

        })

        it("supplies the page size parameter", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middlware = new PageMiddleware({ mode: "page", usePageSize: true, pageSize: 15, pageParameterName: "mypage", pageSizeParameterName: "mypagesize" })
            const requestOptions = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ page: "3" }),
                body: undefined,
            }

            const newRequestOptions = await middlware.beforeFetch(api, requestOptions);

            expect(newRequestOptions).to.eql({
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ mypage: "3", mypagesize: "15" }),
                body: undefined,
            })

        })

        it("maps responses", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            type ApiResult = {
                page: number,
                total: number,
                data: Array<string>
            }

            const mapResult = async (data: unknown): Promise<Page<unknown>> => {
                const typedData = data as ApiResult;
                return {
                    page: {
                        current: (typedData.page),
                        // previous: (typedData.page > 1) ? typedData.page - 1 : null,
                        // next: (typedData.page < typedData.total) ? typedData.page + 1 : null,
                        previous: typedData.page - 1,
                        next: null,
                        last: typedData.total,
                    },
                    items: typedData.data
                }
            }

            const middlware = new PageMiddleware({ mode: "page", pageParameterName: "mypage", mapResult })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ mypage: "3" }),
                body: undefined,
            }
            const result = {
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                page: {
                    current: 3,
                    previous: 2,
                    next: null,
                    last: 3
                },
                items: ["foo", "bar"]
            })

        })


        it("does not map responses when there is no result mapper", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            const middlware = new PageMiddleware({ mode: "page", pageParameterName: "mypage" })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ mypage: "3" }),
                body: undefined,
            }
            const result = {
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            })

        })

        it("does not map responses that do not have the page parameter", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            const mapResultSpy = chai.spy();
            const mapResult = (mapResultSpy as unknown) as ((object: unknown) => Promise<Page<unknown>>);

            const middlware = new PageMiddleware({ mode: "page", pageParameterName: "mypage", mapResult })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ not_the_page_parameter: "3" }),
                body: undefined,
            }
            const result = {
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            })

            expect(mapResultSpy).to.not.be.called;
        })


        it("does not map responses when query parameters are undefined", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            const mapResultSpy = chai.spy();
            const mapResult = (mapResultSpy as unknown) as ((object: unknown) => Promise<Page<unknown>>);

            const middlware = new PageMiddleware({ mode: "page", pageParameterName: "mypage", mapResult })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: undefined,
                body: undefined,
            }
            const result = {
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                page: 3,
                total: 3,
                data: [
                    "foo",
                    "bar"
                ]
            })

            expect(mapResultSpy).to.not.be.called;
        })

    })


    describe("Offset Mode", () => {


        it("does nothing when no page is supplied", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middlware = new PageMiddleware({ mode: "offset", pageSize: 5, offsetParameterName: "myoffset", pageSizeParameterName: "mypagesize" })
            const requestOptions = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ foo: "bar", baz: "bop" }),
                body: undefined,
            }

            const newRequestOptions = await middlware.beforeFetch(api, requestOptions);

            expect(newRequestOptions).to.eql({
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ foo: "bar", baz: "bop" }),
                body: undefined,
            })

        })


        it("does nothing when query parameters are undefined", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middlware = new PageMiddleware({ mode: "offset", pageSize: 5, offsetParameterName: "myoffset", pageSizeParameterName: "mypagesize" })
            const requestOptions = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: undefined,
                body: undefined,
            }

            const newRequestOptions = await middlware.beforeFetch(api, requestOptions);

            expect(newRequestOptions).to.eql({
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: undefined,
                body: undefined,
            })

        })

        it("supplies the offset and page size parameters", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middlware = new PageMiddleware({ mode: "offset", pageSize: 5, offsetParameterName: "myoffset", pageSizeParameterName: "mypagesize" })
            const requestOptions = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ page: "3" }),
                body: undefined,
            }

            const newRequestOptions = await middlware.beforeFetch(api, requestOptions);

            expect(newRequestOptions).to.eql({
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ myoffset: "15", mypagesize: "5" }),
                body: undefined,
            })

        })




        it("maps responses", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            type ApiResult = {
                offset: number,
                size: number,
                total: number,
                data: Array<string>
            }



            const mapResult = async (data: unknown): Promise<Page<unknown>> => {
                const typedData = data as ApiResult;
                const page = Math.floor(typedData.offset / typedData.size) + 1;
                const totalPages = Math.ceil(typedData.total / typedData.size);
                return {
                    page: {
                        current: page,
                        // previous: (page > 1) ? page - 1 : null,
                        // next: (page < totalPages) ? page + 1 : null,
                        previous: page - 1,
                        next: null,
                        last: totalPages,
                    },
                    items: typedData.data
                }
            }

            const middlware = new PageMiddleware({ mode: "offset", pageSize: 5, offsetParameterName: "myoffset", pageSizeParameterName: "mypagesize", mapResult })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ myoffset: "10", mypagesize: "5" }),
                body: undefined,
            }
            const result = {
                offset: 10,
                size: 5,
                total: 15,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                page: {
                    current: 3,
                    previous: 2,
                    next: null,
                    last: 3
                },
                items: ["foo", "bar"]
            })

        })


        it("does not map responses when there is no result mapper", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            const middlware = new PageMiddleware({ mode: "offset", pageSize: 5, offsetParameterName: "myoffset", pageSizeParameterName: "mypagesize" })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ myoffset: "10", mypagesize: "5" }),
                body: undefined,
            }
            const result = {
                offset: 10,
                size: 5,
                total: 15,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                offset: 10,
                size: 5,
                total: 15,
                data: [
                    "foo",
                    "bar"
                ]
            })

        })

        it("does not map responses that do not have the offset parameter", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            const mapResultSpy = chai.spy();
            const mapResult = (mapResultSpy as unknown) as ((object: unknown) => Promise<Page<unknown>>);

            const middlware = new PageMiddleware({ mode: "offset", pageSize: 5, offsetParameterName: "myoffset", pageSizeParameterName: "mypagesize", mapResult })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: new URLSearchParams({ not_my_offset_parameter: "10", mypagesize: "5" }),
                body: undefined,
            }
            const result = {
                offset: 10,
                size: 5,
                total: 15,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                offset: 10,
                size: 5,
                total: 15,
                data: [
                    "foo",
                    "bar"
                ]
            })

            expect(mapResultSpy).to.not.be.called;
        })



        it("does not map responses when query parameters are undefined", async () => {

            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const mockResponse: Response = mock();
            const response = instance(mockResponse);

            const mapResultSpy = chai.spy();
            const mapResult = (mapResultSpy as unknown) as ((object: unknown) => Promise<Page<unknown>>);

            const middlware = new PageMiddleware({ mode: "offset", pageSize: 5, offsetParameterName: "myoffset", pageSizeParameterName: "mypagesize", mapResult })
            const options = {
                method: "STUB",
                uri: "https://www.example.com/stub",
                headers: {},
                queryParameters: undefined,
                body: undefined,
            }
            const result = {
                offset: 10,
                size: 5,
                total: 15,
                data: [
                    "foo",
                    "bar"
                ]
            }

            const newResult = await middlware.afterFetch(api, options, response, result);

            expect(newResult).to.eql({
                offset: 10,
                size: 5,
                total: 15,
                data: [
                    "foo",
                    "bar"
                ]
            })

            expect(mapResultSpy).to.not.be.called;
        })


    })

})

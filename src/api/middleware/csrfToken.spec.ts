import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as chaiFetchMock from "chai-fetch-mock";
import * as sinon from "sinon";
import * as sinonChai from "sinon-chai";
import { mock, instance, when, verify } from "ts-mockito";
import * as Cookie from "js-cookie";
import * as fetchMock from "fetch-mock";
import { Api, CsrfTokenMiddleware } from "../..";

chai.use(chaiAsPromised);
chai.use(chaiFetchMock);
chai.use(sinonChai);
const expect = chai.expect;

const fetch = fetchMock.sandbox();



describe("CSRF Token Middleware", () => {

    describe("Configuration", () => {

        it("has default options", () => {
            const middleware = new CsrfTokenMiddleware();
            expect(middleware.options.cookieProvider).to.equal(Cookie);
            expect(middleware.options.receiveMode).to.equal("cookie")
            expect(middleware.options.receiveCsrfHeaderName).to.equal("x-csrftoken")
            expect(middleware.options.receiveCsrfHeaderPath).to.equal("")
            expect(middleware.options.receiveCsrfCookieName).to.equal("csrftoken")
            expect(middleware.options.receiveCsrfFunction()).to.eventually.be.undefined
            expect(middleware.options.sendMode).to.equal("header")
            expect(middleware.options.sendCsrfFieldName).to.equal("csrf_token")
            expect(middleware.options.sendCsrfHeaderName).to.equal("x-csrftoken")
            expect(middleware.options.sendMethodAllowlist).to.deep.equal(["POST", "PUT", "PATCH", "DELETE"])
            expect(middleware.options.sendMethodBlocklist).to.deep.equal(["GET"])
        })

        it("can be configured with custom options", () => {
            const middleware = new CsrfTokenMiddleware({
                receiveMode: "function",
                receiveCsrfHeaderName: "x-receive-csrf-token",
                receiveCsrfHeaderPath: "my/header/csrf/token",
                receiveCsrfCookieName: "cookie-csrf-token",
                receiveCsrfCookiePath: "my/cookie/csrf/token",
                receiveCsrfFunction: async () => "custom-csrf-token-result",
                sendMode: "body",
                sendCsrfFieldName: "injected_csrf_token",
                sendCsrfHeaderName: "x-send-csrf-token",
                sendMethodAllowlist: ["FOO", "BAR", "BAZ"],
                sendMethodBlocklist: ["BOP", "FLOP", "FLAP"],
            });
            expect(middleware.options.receiveMode).to.equal("function")
            expect(middleware.options.receiveCsrfHeaderName).to.equal("x-receive-csrf-token")
            expect(middleware.options.receiveCsrfHeaderPath).to.equal("my/header/csrf/token")
            expect(middleware.options.receiveCsrfCookieName).to.equal("cookie-csrf-token")
            expect(middleware.options.receiveCsrfCookiePath).to.equal("my/cookie/csrf/token")
            expect(middleware.options.receiveCsrfFunction()).to.eventually.equal("custom-csrf-token-result")
            expect(middleware.options.sendMode).to.equal("body")
            expect(middleware.options.sendCsrfFieldName).to.equal("injected_csrf_token")
            expect(middleware.options.sendCsrfHeaderName).to.equal("x-send-csrf-token")
            expect(middleware.options.sendMethodAllowlist).to.deep.equal(["FOO", "BAR", "BAZ"])
            expect(middleware.options.sendMethodBlocklist).to.deep.equal(["BOP", "FLOP", "FLAP"])
        })

    })

    describe("Request Methods", () => {
        it("Should honour the allowlist", () => {
            const middleware = new CsrfTokenMiddleware({ sendMethodAllowlist: ["A", "B", "C"], sendMethodBlocklist: undefined });
            expect(middleware.shouldSend("A")).to.be.true;
            expect(middleware.shouldSend("B")).to.be.true;
            expect(middleware.shouldSend("C")).to.be.true;
            expect(middleware.shouldSend("D")).to.be.false;
            expect(middleware.shouldSend("E")).to.be.false;
        })

        it("Should honour the blocklist", () => {
            const middleware = new CsrfTokenMiddleware({ sendMethodAllowlist: undefined, sendMethodBlocklist: ["A", "B", "C"] });
            expect(middleware.shouldSend("A")).to.be.false;
            expect(middleware.shouldSend("B")).to.be.false;
            expect(middleware.shouldSend("C")).to.be.false;
            expect(middleware.shouldSend("D")).to.be.true;
            expect(middleware.shouldSend("E")).to.be.true;
        })

        it("Should honour a combined allow and blocklist", () => {
            const middleware = new CsrfTokenMiddleware({ sendMethodAllowlist: ["A", "B", "C"], sendMethodBlocklist: ["C", "D", "E"] });
            expect(middleware.shouldSend("A")).to.be.true;
            expect(middleware.shouldSend("B")).to.be.true;
            expect(middleware.shouldSend("C")).to.be.false;
            expect(middleware.shouldSend("D")).to.be.false;
            expect(middleware.shouldSend("E")).to.be.false;
        })
    })

    describe("Meta Guards", () => {
        it("sets the gettingCsrfToken__beforeFetch meta property when the gettingCsrfToken meta guard is set", async () => {
            const middleware = new CsrfTokenMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const newOptions = await middleware.beforeFetch(api, { method: "STUB", uri: "https://www.example.com/stub", headers: {}, meta: { gettingCsrfToken: true } })
            expect(newOptions.meta).to.have.property("gettingCsrfToken").and.to.be.true;
            expect(newOptions.meta).to.have.property("gettingCsrfToken__beforeFetch").and.to.be.true;
        })
    })

    describe("Modes Off", () => {
        it("does nothing when the send and receive modes are off", async () => {
            fetch.getOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "off",
                sendMode: "off",
                sendMethodAllowlist: ["GET"],
                sendMethodBlocklist: undefined,
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.get("dummy");

            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.be.undefined;
            fetch.restore();
        })
    })

    describe("Receive from Header", () => {
        it("receives the CSRF token from a header when no token is currently set", async () => {
            fetch.getOnce("https://www.example.com/get-csrf-token", {
                status: 200,
                headers: {
                    "x-csrf-token": "stub-token"
                },
                body: {}
            })
            fetch.getOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "header",
                receiveCsrfHeaderPath: "get-csrf-token",
                receiveCsrfHeaderName: "x-csrf-token",
                sendMethodAllowlist: ["STUB"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.get("dummy");

            expect(fetch.called("https://www.example.com/get-csrf-token")).to.be.true;
            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.equal("stub-token")
            fetch.restore();
        })

        it("receives the CSRF token from a header when no token is currently set", async () => {
            fetch.getOnce("https://www.example.com/get-csrf-token", {
                status: 200,
                headers: {
                    "x-csrf-token": "stub-token"
                },
                body: {}
            })
            fetch.getOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "header",
                receiveCsrfHeaderPath: "get-csrf-token",
                receiveCsrfHeaderName: "x-csrf-token",
                sendMethodAllowlist: ["STUB"],
                sendMethodBlocklist: undefined
            });

            middleware.csrfToken = "preset-csrf-token";

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.get("dummy");

            expect(fetch.called("https://www.example.com/get-csrf-token")).to.be.false;
            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.equal("preset-csrf-token")
            fetch.restore();
        })


        it("always receives the latest CSRF token if supplied in a header", async () => {
            fetch.getOnce("https://www.example.com/get-csrf-token", {
                status: 200,
                headers: {
                    "x-csrf-token": "stub-token"
                },
                body: {}
            })
            fetch.getOnce("https://www.example.com/dummy", {
                status: 200,
                headers: {
                    "x-csrf-token": "latest-token"
                },
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "header",
                receiveCsrfHeaderPath: "get-csrf-token",
                receiveCsrfHeaderName: "x-csrf-token",
                sendMethodAllowlist: ["STUB"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.get("dummy");

            expect(fetch.called("https://www.example.com/get-csrf-token")).to.be.true;
            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.equal("latest-token")
            fetch.restore();
        })
    })

    describe("Receive from Cookie", () => {

        it("receives the csrf token from a cookie", async () => {
            fetch.getOnce("https://www.example.com/get-csrf-token", {
                status: 200,
                headers: {
                    "set-cookie": "csrf-cookie-name=stub-cookie-unused-in-test"
                },
                body: {}
            })
            fetch.getOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const CookieMock: Cookie.CookiesStatic = mock<Cookie.CookiesStatic>();
            const cookieProvider = instance(CookieMock);

            const middleware = new CsrfTokenMiddleware({
                cookieProvider,
                receiveMode: "cookie",
                receiveCsrfCookieName: "csrf-cookie-name",
                receiveCsrfCookiePath: "get-csrf-token",
                sendMethodAllowlist: ["STUB"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            when(CookieMock.get("csrf-cookie-name")).thenReturn("csrf-cookie-token");

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.get("dummy");

            verify(CookieMock.get("csrf-cookie-name")).once();
            expect(fetch.called("https://www.example.com/get-csrf-token")).to.be.true;
            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.equal("csrf-cookie-token")
            fetch.restore();
        })

        it("maintains the existing csrf token if the cookie token goes away", async () => {
            fetch.getOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const CookieMock: Cookie.CookiesStatic = mock<Cookie.CookiesStatic>();
            const cookieProvider = instance(CookieMock);

            const middleware = new CsrfTokenMiddleware({
                cookieProvider,
                receiveMode: "cookie",
                receiveCsrfCookieName: "csrf-cookie-name",
                sendMethodAllowlist: ["STUB"],
                sendMethodBlocklist: undefined
            });

            middleware.csrfToken = "existing-csrf-token"

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            when(CookieMock.get("csrf-cookie-name")).thenReturn(undefined);

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.get("dummy");

            verify(CookieMock.get("csrf-cookie-name")).once();
            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.equal("existing-csrf-token")
            fetch.restore();
        })

    })

    describe("Receive the csrf token from function", () => {

        it("receives the csrf token using the custom function", async () => {

            fetch.getOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "function",
                receiveCsrfFunction: async () => Promise.resolve("custom-function-csrf-token"),
                sendMethodAllowlist: ["STUB"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.get("dummy");

            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.equal("custom-function-csrf-token")
            fetch.restore();
        });


        it("uses the existing csrf token if the custom function result is undefined", async () => {

            fetch.getOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "function",
                receiveCsrfFunction: async () => undefined,
                sendMethodAllowlist: ["STUB"],
                sendMethodBlocklist: undefined
            });

            middleware.csrfToken = "existing-csrf-token-value"

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.get("dummy");

            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.equal("existing-csrf-token-value")
            fetch.restore();
        });

    })

    describe("Unsupported Receive method", () => {
        it("does nothing when the supplied receive mode is unsupported", async () => {
            const consoleWarnStub = sinon.stub(console, "warn");

            fetch.getOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const receiveMode = JSON.parse(JSON.stringify("stub-mode"))
            const middleware = new CsrfTokenMiddleware({
                receiveMode: receiveMode,
                sendMode: "off",
                sendMethodAllowlist: ["STUB"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.get("dummy");

            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.be.undefined;
            expect(consoleWarnStub.calledOnceWith("The CSRF Token Middleware receive mode 'stub-mode' is not supported")).to.be.true;

            fetch.restore();
            consoleWarnStub.restore();
        })
    })

    describe("Send HTTP Header", () => {

        it("sends a CSRF token as a http header", async () => {
            fetch.postOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "function",
                receiveCsrfFunction: async () => Promise.resolve("preset-token"),
                sendMode: "header",
                sendCsrfHeaderName: "x-stub-send-csrf-token",
                sendMethodAllowlist: ["POST"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.create("dummy", {});

            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(fetch.lastOptions().headers).has.property("x-stub-send-csrf-token", "preset-token")

            fetch.restore();
        })

        it("sends an empty string CSRF token as a http header if the token is undefined", async () => {
            fetch.postOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "function",
                receiveCsrfFunction: async () => Promise.resolve(undefined),
                sendMode: "header",
                sendCsrfHeaderName: "x-stub-send-csrf-token",
                sendMethodAllowlist: ["POST"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.create("dummy", {});

            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(fetch.lastOptions().headers).has.property("x-stub-send-csrf-token", "")

            fetch.restore();
        })

    })


    describe("Send HTTP Body", () => {

        it("sends a CSRF token as a http body property", async () => {
            fetch.postOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "function",
                receiveCsrfFunction: async () => Promise.resolve("preset-token"),
                sendMode: "body",
                sendCsrfFieldName: "stub_csrf_token",
                sendMethodAllowlist: ["POST"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.create("dummy", {});

            expect(fetch.called("https://www.example.com/dummy", {
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    "accept": "application/json",
                },
                body: { "stub_csrf_token": "preset-token" }
            })).to.be.true;

            fetch.restore();
        })


        it("sends a CSRF token as a http body property when no body is defined", async () => {
            fetch.postOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "function",
                receiveCsrfFunction: async () => Promise.resolve("preset-token"),
                sendMode: "body",
                sendCsrfFieldName: "stub_csrf_token",
                sendMethodAllowlist: ["POST"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.create("dummy", undefined);

            expect(fetch.called("https://www.example.com/dummy", {
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    "accept": "application/json",
                },
                body: { "stub_csrf_token": "preset-token" }
            })).to.be.true;

            fetch.restore();
        })


        it("sends an empty string CSRF token as a http body property when the csrf token is undefined", async () => {
            fetch.postOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const middleware = new CsrfTokenMiddleware({
                receiveMode: "function",
                receiveCsrfFunction: async () => Promise.resolve(undefined),
                sendMode: "body",
                sendCsrfFieldName: "stub_csrf_token",
                sendMethodAllowlist: ["POST"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.create("dummy", {});

            expect(fetch.called("https://www.example.com/dummy", {
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    "accept": "application/json",
                },
                body: { "stub_csrf_token": "" }
            })).to.be.true;

            fetch.restore();
        })

    })

    describe("Unsupported Send method", () => {
        it("does nothing when the supplied send mode is unsupported", async () => {
            const consoleWarnStub = sinon.stub(console, "warn");

            fetch.postOnce("https://www.example.com/dummy", {
                status: 200,
                body: {}
            })

            const sendMode = JSON.parse(JSON.stringify("stub-mode"))
            const middleware = new CsrfTokenMiddleware({
                receiveMode: "off",
                sendMode: sendMode,
                sendMethodAllowlist: ["POST"],
                sendMethodBlocklist: undefined
            });

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            // Perform a dummy API request.
            // This causes the API to perform middleware before- and after-fetch operations,
            // which will include the CSRF Token middleware's operations.
            await api.create("dummy", {});

            expect(fetch.called("https://www.example.com/dummy")).to.be.true;
            expect(middleware.csrfToken).to.be.undefined;
            expect(consoleWarnStub.calledOnceWith("The CSRF Token Middleware send mode 'stub-mode' is not supported")).to.be.true;

            fetch.restore();
            consoleWarnStub.restore();
        })
    })


})
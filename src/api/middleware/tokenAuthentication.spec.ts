import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as chaiFetchMock from "chai-fetch-mock";
import * as fetchMock from "fetch-mock";
import { AuthenticationApiError } from "../..";
import { TokenAuthenticationMiddleware } from "./tokenAuthentication"
import { Api } from "../api";

chai.use(chaiAsPromised);
chai.use(chaiFetchMock);
const expect = chai.expect;

const fetch = fetchMock.sandbox();


describe("Token Authentication Middleware", () => {

    describe("Authentication Attempts", () => {


        it("authenticates with a username and password", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "abcd", refresh: "efgh" }))

            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "password", { "username": "jimbo", "password": "goat" });

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.equal("abcd");
            expect(middleware.refreshToken).to.equal("efgh");

            fetch.restore();
        })

        it("fails to authenticate with a username and password", async () => {
            fetch.post(
                "https://www.example.com/login",
                {
                    status: 400,
                    body: { detail: "The username or password is incorrect" }
                }
            )

            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const promise = middleware.authenticate(api, "password", { "username": "jimbo", "password": "goat" });
            expect(promise).to.eventually.be.rejectedWith("Authentication attempt failed: Bad Request")
                .and.be.an.instanceOf(AuthenticationApiError)
                .and.have.property("data", { detail: "The username or password is incorrect" });

            fetch.restore();
        })

        it("fails to authenticate with no username or password", async () => {
            fetch.post(
                "https://www.example.com/login",
                {
                    status: 400,
                    body: { detail: "The username and password were not supplied" }
                }
            )

            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const promise = middleware.authenticate(api, "password", {});
            expect(promise).to.eventually.be.rejectedWith("Authentication attempt failed: Bad Request")
                .and.be.an.instanceOf(AuthenticationApiError)
                .and.have.property("data", { detail: "The username and password were not supplied" });

            fetch.restore();
        })


        it("fails to authenticate with no payload for password authentication", async () => {
            fetch.post(
                "https://www.example.com/login",
                {
                    status: 400,
                    body: { detail: "The username and password were not supplied" }
                }
            )

            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const promise = middleware.authenticate(api, "password", undefined);
            expect(promise).to.eventually.be.rejectedWith("Authentication attempt failed: Bad Request")
                .and.be.an.instanceOf(AuthenticationApiError)
                .and.have.property("data", { detail: "The username and password were not supplied" });

            fetch.restore();
        })

        it("authenticates with an access token", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "token", { access: "woei" });

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.equal("woei");
            expect(middleware.refreshToken).to.be.undefined;
        })

        it("authenticates with an access and refresh token", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "token", { access: "woei", refresh: "sldk" });

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.equal("woei");
            expect(middleware.refreshToken).to.equal("sldk");
        })


        it("authenticates with an access and refresh token and an expiry stamp", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "token", { access: "woei", refresh: "sldk", expiresAt: 12345 });

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.equal("woei");
            expect(middleware.refreshToken).to.equal("sldk");
            expect(middleware.expiresAt).to.equal(12345);
        })

        it("authenticates with no access or refresh token", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "token", {});

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.be.undefined;
            expect(middleware.refreshToken).to.be.undefined;
        })

        it("authenticates with no payload for token authentication", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "token", undefined);

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.be.undefined;
            expect(middleware.refreshToken).to.be.undefined;
        })

        it("authenticates with an API key", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "key", { key: "kenobi" });

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.equal("kenobi");
            expect(middleware.refreshToken).to.be.undefined;
        })

        it("authenticates with no API key", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "key", {});

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.be.undefined;
            expect(middleware.refreshToken).to.be.undefined;
        })


        it("authenticates with no payload for key authentication", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "key", undefined);

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.be.undefined;
            expect(middleware.refreshToken).to.be.undefined;
        })

        it("does not handle unknown authentication types", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "unknown", {});

            expect(handled).to.be.false;
            expect(middleware.bearerToken).to.be.undefined;
            expect(middleware.refreshToken).to.be.undefined;
        })

    })


    describe("Deuthentication Attempts", () => {

        it("deauthenticates with a username and password", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "abcd", refresh: "efgh" }))

            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "password", { "username": "jimbo", "password": "goat" });

            expect(handled).to.be.true;
            expect(middleware.bearerToken).to.equal("abcd");
            expect(middleware.refreshToken).to.equal("efgh");

            const handledDeauth = await middleware.deauthenticate(api);
            expect(handledDeauth).to.be.true;
            expect(middleware.bearerToken).to.be.undefined;
            expect(middleware.refreshToken).to.be.undefined;
            expect(middleware.expiresAt).to.be.undefined;

            fetch.restore();
        })

        it("does not deauthenticate if there is no bearer or refresh token", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handledDeauth = await middleware.deauthenticate(api);
            expect(handledDeauth).to.be.false;
            expect(middleware.bearerToken).to.be.undefined;
            expect(middleware.refreshToken).to.be.undefined;
        })

        it("deauthenticates if there is an access but no refesh token", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])
            await api.authenticate("token", { access: "foo", expiresAt: 12345 })

            const handledDeauth = await middleware.deauthenticate(api);
            expect(handledDeauth).to.be.true;
            expect(middleware.bearerToken).to.be.undefined;
            expect(middleware.refreshToken).to.be.undefined;
            expect(middleware.expiresAt).to.be.undefined;
        })

        it("deauthenticates if there is a refresh but no access token", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])
            await api.authenticate("token", { refresh: "foo", expiresAt: 12345 })

            const handledDeauth = await middleware.deauthenticate(api);
            expect(handledDeauth).to.be.true;
            expect(middleware.bearerToken).to.be.undefined;
            expect(middleware.refreshToken).to.be.undefined;
            expect(middleware.expiresAt).to.be.undefined;
        })

    })

    describe("Bearer Token Header Injection", () => {

        it("does not inject a bearer token if one is not available", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: {} });
        })

        it("injects an acquired bearer access token", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "huehuehue" }))

            const middleware = new TokenAuthenticationMiddleware({ passwordLoginPath: "login" });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "password", { username: "samantha", password: "orange" });
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });

            expect(handled).to.be.true;
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { authorization: "Bearer huehuehue" } });

            fetch.restore();
        })

        it("injects a pre-configured bearer access token", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "token", { access: "honeysuckle" });
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });

            expect(handled).to.be.true;
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { authorization: "Bearer honeysuckle" } });
        })

        it("injects an API key", async () => {
            const middleware = new TokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "key", { key: "kenobi" });
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });

            expect(handled).to.be.true;
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { authorization: "Bearer kenobi" } });
        })

    })

    describe("Token Expiry", () => {

        it("recognises an undefined access token as for immediate renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const token = undefined;
            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;
        })

        it("recognises a null access token as for immediate renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const token = null;

            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;
        })


        it("recognises an empty access token as for immediate renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const token = "";

            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;
        })

        it("recognises a token that is not yet due for renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now + 10.0;
            const token = "." + Buffer.from(JSON.stringify({ exp }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.false;
        })

        it("recognises a token not yet expired but due for renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now + 1.0;
            const token = "." + Buffer.from(JSON.stringify({ exp }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;
        })

        it("recognises a token that has expired and is due for renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now - 1.0;
            const token = "." + Buffer.from(JSON.stringify({ exp }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;
        })

        it("treats an undefined access token as expired", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "token", { access: undefined, expiresAt: undefined });

            expect(handled).to.be.true;
            expect(middleware.expiresAt).to.be.undefined;
            expect(middleware.isDueForRenewal).to.be.true;
        })

        it("recognises a token that has expired and is due for renewal when no refresh buffer is supplied", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: undefined });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now - 1.0;
            const token = "." + Buffer.from(JSON.stringify({ exp }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;
        })

        it("assumes that a token without an exp claim does not require renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const token = "." + Buffer.from(JSON.stringify({ foo: "bar", baz: "bop" }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.false;
        })

        it("assumes that a token which is not a JWT does not require renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const token = "hello world";

            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.false;
        })

        it("assumes that a token which is not a JWT but looks like a JWT does not require renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const token = "hello world . there are periods to make this look like a JWT . goodbye world";

            const handled = await middleware.authenticate(api, "token", { access: token });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.false;
        })

        it("recognises manually set expiry that is not yet expired but due for renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const expiresAt = now + 1.0;

            const handled = await middleware.authenticate(api, "token", { expiresAt });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;
        })

        it("recognises manually set expiry that has expired and is due for renewal", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const expiresAt = now + - 10.0;

            const handled = await middleware.authenticate(api, "token", { expiresAt });

            expect(handled).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;
        })

    })

    describe("Token Refresh", () => {
        it("cannot renew without a refresh token", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now - 1.0;
            const token = "." + Buffer.from(JSON.stringify({ type: "access", exp }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token, refresh: undefined });

            expect(handled).to.be.true;
            expect(middleware.canRenew).to.be.false;
        })

        it("can renew with a refresh token", async () => {
            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0 });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now - 1.0;
            const token = "." + Buffer.from(JSON.stringify({ type: "access", exp }), "utf-8").toString("base64");
            const refreshToken = "." + Buffer.from(JSON.stringify({ type: "refresh" }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token, refresh: refreshToken });

            expect(handled).to.be.true;
            expect(middleware.canRenew).to.be.true;
        })

        it("does not refresh a token which is not due for renewal", async () => {
            fetch.post("https://www.example.com/token/renew", { access: "renewed-access-token" });

            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0, refreshTokenPath: "token/renew" });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now + 10.0;
            const token = "." + Buffer.from(JSON.stringify({ type: "access", exp }), "utf-8").toString("base64");
            const refreshToken = "." + Buffer.from(JSON.stringify({ type: "refresh" }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token, refresh: refreshToken });

            expect(handled).to.be.true;
            expect(middleware.canRenew).to.be.true;
            expect(middleware.isDueForRenewal).to.be.false;

            expect(middleware.bearerToken).to.eql(token);

            fetch.restore();
        })

        it("refreshes a token which is due for renewal", async () => {
            fetch.post("https://www.example.com/token/renew", { access: "renewed-access-token" });

            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0, refreshTokenPath: "token/renew" });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now - 1.0;
            const token = "." + Buffer.from(JSON.stringify({ type: "access", exp }), "utf-8").toString("base64");
            const refreshToken = "." + Buffer.from(JSON.stringify({ type: "refresh" }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token, refresh: refreshToken });

            expect(handled).to.be.true;
            expect(middleware.canRenew).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;

            await middleware.refresh(api);

            expect(middleware.bearerToken).to.eql("renewed-access-token");

            fetch.restore();
        })

        it("fails to refresh a token which is due for renewal", async () => {
            fetch.post(
                "https://www.example.com/token/renew",
                {
                    status: 400,
                    body: { detail: "The refresh token is not valid" }
                }
            )

            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0, refreshTokenPath: "token/renew" });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now - 1.0;
            const token = "." + Buffer.from(JSON.stringify({ type: "access", exp }), "utf-8").toString("base64");
            const refreshToken = "." + Buffer.from(JSON.stringify({ type: "refresh" }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token, refresh: refreshToken });

            expect(handled).to.be.true;
            expect(middleware.canRenew).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;

            const promise = middleware.refresh(api);
            expect(promise).to.eventually.be.rejectedWith("Authentication attempt failed: Bad Request")
                .and.be.an.instanceOf(AuthenticationApiError)
                .and.have.property("data", { detail: "The refresh token is not valid" });

            fetch.restore();
        })

        it("refreshes a token which is due for renewal using a token expiry key", async () => {
            fetch.post("https://www.example.com/token/renew", { access: "renewed-access-token", expires: 554433 });

            const middleware = new TokenAuthenticationMiddleware({ refreshBufferSeconds: 5.0, refreshTokenPath: "token/renew", refreshTokenExpiresAtKey: "expires" });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const now = Math.round(Date.now() / 1000.0);
            const exp = now - 1.0;
            const token = "." + Buffer.from(JSON.stringify({ type: "access", exp }), "utf-8").toString("base64");
            const refreshToken = "." + Buffer.from(JSON.stringify({ type: "refresh" }), "utf-8").toString("base64");

            const handled = await middleware.authenticate(api, "token", { access: token, refresh: refreshToken });

            expect(handled).to.be.true;
            expect(middleware.canRenew).to.be.true;
            expect(middleware.isDueForRenewal).to.be.true;

            await middleware.refresh(api);

            expect(middleware.bearerToken).to.eql("renewed-access-token");
            expect(middleware.expiresAt).to.equal(554433);

            fetch.restore();
        })

    })

})
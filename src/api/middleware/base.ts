import { ApiMiddlware, CrudApi, RequestOptions } from "../types";

export class BaseMiddleware implements ApiMiddlware {

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async authenticate(api: CrudApi, type: string, payload?: { [key: string]: unknown }): Promise<boolean> {
        return false;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async deauthenticate(api: CrudApi): Promise<boolean> {
        return false;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async beforeFetch(api: CrudApi, options: RequestOptions): Promise<RequestOptions> {
        return options;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async afterFetch<ResponsePayloadType>(api: CrudApi, options: RequestOptions, response: Response, result: ResponsePayloadType | unknown): Promise<ResponsePayloadType | unknown> {
        return result;
    }

}

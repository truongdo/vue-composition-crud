import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as chaiFetchMock from "chai-fetch-mock";
import { mock, instance } from "ts-mockito";
import { BaseMiddleware } from "./base"
import { CrudApi } from "../types";

chai.use(chaiAsPromised);
chai.use(chaiFetchMock);
const expect = chai.expect;



describe("Base Middleware", () => {

    it("does not alter the request options by default", async () => {
        const mockApi: CrudApi = mock();
        const api = instance(mockApi);

        const middleware = new BaseMiddleware();
        const promise = middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: { a: "foo", b: "bar", c: "999" } })
        await expect(promise).to.eventually.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { a: "foo", b: "bar", c: "999" } })
    })

    it("does not alter the response payload by default", async () => {
        const mockApi: CrudApi = mock();
        const api = instance(mockApi);

        const mockResponse: Response = mock();
        const response = instance(mockResponse);

        const middleware = new BaseMiddleware();
        const promise = middleware.afterFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} }, response, { qwer: "asdf", zxcv: "poui" });
        await expect(promise).to.eventually.eql({ qwer: "asdf", zxcv: "poui" })
    })

    it("does not supply authentication behaviour by default", async () => {
        const mockApi: CrudApi = mock();
        const api = instance(mockApi);

        const middleware = new BaseMiddleware();
        const handled = await middleware.authenticate(api, "fobar", { "baz": "bop" });
        expect(handled).to.be.false;
    })

    it("does not supply deauthentication behaviour by default", async () => {
        const mockApi: CrudApi = mock();
        const api = instance(mockApi);

        const middleware = new BaseMiddleware();
        const handled = await middleware.deauthenticate(api);
        expect(handled).to.be.false;
    })
})
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as chaiFetchMock from "chai-fetch-mock";
import * as fetchMock from "fetch-mock";
import crossFetch from "cross-fetch";
import { anything, instance, mock, verify, when, deepEqual } from "ts-mockito"
import { AuthenticationApiError, GenericApiError } from "..";
import { Api } from "./api"
import { ApiMiddlware, RequestOptions } from "./types"
import * as sinon from "sinon";

chai.use(chaiAsPromised);
chai.use(chaiFetchMock);
const expect = chai.expect;

const fetch = fetchMock.sandbox();


describe("API", () => {

    describe("Configuration", () => {
        it("has default configuration options", () => {
            const api = new Api();
            expect(api.fetchMethod).to.eql(crossFetch);
            expect(api.baseUrl).to.eql("/");
            expect(api.options.fetchOptions).to.deep.equal({
                credentials: "same-origin",
                cache: "default",
                mode: "cors",
                redirect: "follow",
                referrerPolicy: "no-referrer-when-downgrade",
            })
        })

        it("accepts fetch options", () => {
            const api = new Api({
                fetchOptions: {
                    credentials: "omit",
                    cache: "no-cache",
                    mode: "same-origin",
                    redirect: "error",
                    referrerPolicy: "origin"
                }
            })
            expect(api.options.fetchOptions).to.deep.equal({
                credentials: "omit",
                cache: "no-cache",
                mode: "same-origin",
                redirect: "error",
                referrerPolicy: "origin"
            })
        })

        it("can reconfigure fetch options", () => {
            const api = new Api({
                fetchOptions: {
                    credentials: "omit",
                    cache: "no-cache",
                    mode: "same-origin",
                    redirect: "error",
                    referrerPolicy: "origin"
                }
            })
            expect(api.options.fetchOptions).to.deep.equal({
                credentials: "omit",
                cache: "no-cache",
                mode: "same-origin",
                redirect: "error",
                referrerPolicy: "origin"
            })

            api.configure({ fetchOptions: { redirect: "manual" } })
            expect(api.options.fetchOptions).to.deep.equal({
                credentials: "omit",
                cache: "no-cache",
                mode: "same-origin",
                redirect: "manual",
                referrerPolicy: "origin"
            })
        })

        it("can uses default values with undefined fetch options", () => {
            const api = new Api({ fetchOptions: undefined })
            expect(api.options.fetchOptions).to.deep.equal({
                credentials: "same-origin",
                cache: "default",
                mode: "cors",
                redirect: "follow",
                referrerPolicy: "no-referrer-when-downgrade",
            })
        })

        it("throws an exception if fetch is not supplied", async () => {
            const api = new Api({ fetch: undefined, baseUrl: "https://www.example.com/" })
            const promise = api.list("stub")
            expect(promise).to.eventually.be.rejectedWith("A fetch provider is not defined")
                .and.to.be.an.instanceOf(GenericApiError);
        })

    })

    describe("Authentication", () => {
        it("does not handle authentication by default", async () => {
            const api = new Api({ fetch });
            const authHandler = sinon.spy();
            api.on("authenticated", authHandler);
            const promise = api.authenticate("stub", {});
            await expect(promise).to.eventually.be.rejectedWith("There is no registered authentication handler for the authentication type 'stub'")
                .and.be.instanceof(AuthenticationApiError);
            expect(authHandler.notCalled).to.be.true;
        })
    })

    describe("Deauthentication", () => {
        it("does not handle deauthentication by default", async () => {
            const api = new Api({ fetch });
            const deauthHandler = sinon.spy();
            api.on("deauthenticated", deauthHandler);
            const promise = api.deauthenticate();
            await expect(promise).to.eventually.be.rejectedWith("No handlers indicated that they performed a deauthentication operation successfully")
                .and.be.instanceof(AuthenticationApiError);
            expect(deauthHandler.notCalled).to.be.true;
        })
    })

    describe("Deauthentication", () => {
        it("rejects when a middleware encounters a failure", async () => {
            const Middleware = mock<ApiMiddlware>();
            when(Middleware.deauthenticate(anything())).thenReject(new Error("Some deauthentication failure"))
            const middleware = instance(Middleware);

            const api = new Api({ fetch }, [middleware]);
            const deauthHandler = sinon.spy();
            api.on("deauthenticated", deauthHandler);
            const promise = api.deauthenticate();
            await expect(promise).to.eventually.be.rejectedWith("One or more deauthentication handlers failed to run successfully")
                .and.be.instanceof(AuthenticationApiError);
            expect(deauthHandler.notCalled).to.be.true;
        })
    })

    describe("Deauthentication", () => {
        it("succeeds when one or more middlewares report deauthentication has been handled", async () => {
            const Middleware = mock<ApiMiddlware>();
            when(Middleware.deauthenticate(anything())).thenResolve(true)
            const middleware = instance(Middleware);

            const api = new Api({ fetch }, [middleware]);
            const deauthHandler = sinon.spy();
            api.on("deauthenticated", deauthHandler);
            const promise = api.deauthenticate();
            await expect(promise).to.eventually.be.true;
            expect(deauthHandler.called).to.be.true;
        })
    })

    describe("Middleware", () => {
        it("runs authentication middleware until handled", async () => {
            const MiddlewareA = mock<ApiMiddlware>();
            const MiddlewareB = mock<ApiMiddlware>();
            const MiddlewareC = mock<ApiMiddlware>();

            const middlewares = [
                instance(MiddlewareA),
                instance(MiddlewareB),
                instance(MiddlewareC)
            ]

            const api = new Api({ fetch }, middlewares);
            const authHandler = sinon.spy();
            api.on("authenticated", authHandler);

            when(MiddlewareA.authenticate(api, "stub", anything())).thenResolve(false);
            when(MiddlewareB.authenticate(api, "stub", anything())).thenResolve(true);
            when(MiddlewareC.authenticate(api, "stub", anything())).thenThrow(new Error("Middleware C authenticate call should not be reached"));

            const promise = api.authenticate("stub", {})
            await expect(promise).to.eventually.be.true;
            expect(authHandler.calledOnce).to.be.true;

            verify(MiddlewareA.authenticate(api, "stub", anything())).once();
            verify(MiddlewareB.authenticate(api, "stub", anything())).once();
            verify(MiddlewareC.authenticate(api, "stub", anything())).never();
        })

        it("runs all pre-fetch and post-fetch middleware", async () => {
            fetch.getOnce("https://www.example.com/stub", [{ id: 1, name: "sally" }, { id: 2, name: "george" }]);

            const MiddlewareA = mock<ApiMiddlware>();
            const MiddlewareB = mock<ApiMiddlware>();
            const MiddlewareC = mock<ApiMiddlware>();

            const middlewares = [
                instance(MiddlewareA),
                instance(MiddlewareB),
                instance(MiddlewareC)
            ]

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, middlewares);

            when(MiddlewareA.beforeFetch(api, anything()))
                .thenCall((api, options) => Promise.resolve({ ...options, headers: { ...options.headers, "a": "foo" } }));
            when(MiddlewareB.beforeFetch(api, anything()))
                .thenCall((api, options) => Promise.resolve({ ...options, headers: { ...options.headers, "b": "bar" } }));
            when(MiddlewareC.beforeFetch(api, anything()))
                .thenCall((api, options) => Promise.resolve({ ...options, headers: { ...options.headers, "c": "baz" } }));

            when(MiddlewareA.afterFetch(api, anything(), anything(), anything()))
                .thenCall((api, options, response, result) => Promise.resolve([...result, { id: 3, name: "middleware-a" }]));
            when(MiddlewareB.afterFetch(api, anything(), anything(), anything()))
                .thenCall((api, options, response, result) => Promise.resolve([...result, { id: 4, name: "middleware-b" }]));
            when(MiddlewareC.afterFetch(api, anything(), anything(), anything()))
                .thenCall((api, options, response, result) => Promise.resolve([...result, { id: 5, name: "middleware-c" }]));

            const promise = api.list("stub")
            await expect(promise).to.eventually.eql([
                { id: 1, name: "sally" },
                { id: 2, name: "george" },
                { id: 3, name: "middleware-a" },
                { id: 4, name: "middleware-b" },
                { id: 5, name: "middleware-c" }
            ]);

            verify(MiddlewareA.beforeFetch(api, deepEqual({
                method: "GET",
                uri: "https://www.example.com/stub",
                queryParameters: undefined,
                headers: { "content-type": "application/json", "accept": "application/json" },
                payload: undefined,
                credentials: "same-origin",
                cache: "default",
                mode: "cors",
                redirect: "follow",
                referrerPolicy: "no-referrer-when-downgrade",
                meta: undefined
            }))).once();
            verify(MiddlewareB.beforeFetch(api, deepEqual({
                method: "GET",
                uri: "https://www.example.com/stub",
                queryParameters: undefined,
                headers: { "content-type": "application/json", "accept": "application/json", "a": "foo" },
                payload: undefined,
                credentials: "same-origin",
                cache: "default",
                mode: "cors",
                redirect: "follow",
                referrerPolicy: "no-referrer-when-downgrade",
                meta: undefined
            }))).once();
            verify(MiddlewareC.beforeFetch(api, deepEqual({
                method: "GET", uri: "https://www.example.com/stub",
                queryParameters: undefined,
                headers: { "content-type": "application/json", "accept": "application/json", "a": "foo", "b": "bar" },
                payload: undefined,
                credentials: "same-origin",
                cache: "default",
                mode: "cors",
                redirect: "follow",
                referrerPolicy: "no-referrer-when-downgrade",
                meta: undefined
            }))).once();

            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

    })

    describe("Request Parameters", () => {


        it("supplies default request options", async () => {
            fetch.getOnce(
                "https://www.example.com/stub",
                [{ id: 1, name: "sally" }, { id: 2, name: "george" }]
            );

            const Middleware = mock<ApiMiddlware>();
            const middleware = instance(Middleware);
            const middlewares = [middleware];

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, middlewares);

            when(Middleware.beforeFetch(api, anything()))
                .thenCall((api, options: RequestOptions) => {
                    return Promise.resolve({
                        ...options,
                        credentials: undefined,
                        cache: undefined,
                        mode: undefined,
                        redirect: undefined,
                        referrerPolicy: undefined,
                    })
                });

            when(Middleware.afterFetch(api, anything(), anything(), anything()))
                .thenCall((api, options, response, result) => {
                    expect(options).to.deep.equal({
                        queryParameters: undefined,
                        uri: "https://www.example.com/stub",
                        method: "GET",
                        headers: {
                            "content-type": "application/json",
                            "accept": "application/json",
                        },
                        payload: undefined,
                        credentials: "same-origin",
                        cache: "default",
                        mode: "cors",
                        redirect: "follow",
                        referrerPolicy: "no-referrer-when-downgrade",
                        meta: undefined
                    })
                    return Promise.resolve(result);
                });

            const promise = api.list("stub")
            await expect(promise).to.eventually.eql([
                { id: 1, name: "sally" },
                { id: 2, name: "george" },
            ]);

            expect(fetch.done()).to.be.true;
            verify(middleware).called;
            fetch.reset();
        })

        it("supplies custom request options", async () => {
            fetch.getOnce(
                "https://www.example.com/stub",
                [{ id: 1, name: "sally" }, { id: 2, name: "george" }]
            );

            const Middleware = mock<ApiMiddlware>();
            const middleware = instance(Middleware);
            const middlewares = [middleware];

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, middlewares);

            when(Middleware.beforeFetch(api, anything()))
                .thenCall((api, options: RequestOptions) => {
                    return Promise.resolve({
                        ...options,
                        credentials: "include",
                        cache: "no-cache",
                        mode: "no-cors",
                        redirect: "manual",
                        referrerPolicy: "origin"
                    })
                });

            when(Middleware.afterFetch(api, anything(), anything(), anything()))
                .thenCall((api, options, response, result) => {
                    expect(options).to.deep.equal({
                        queryParameters: undefined,
                        uri: "https://www.example.com/stub",
                        method: "GET",
                        headers: {
                            "content-type": "application/json",
                            "accept": "application/json",
                        },
                        payload: undefined,
                        credentials: "include",
                        cache: "no-cache",
                        mode: "no-cors",
                        redirect: "manual",
                        referrerPolicy: "origin",
                        meta: undefined,
                    })
                    return Promise.resolve(result);
                });

            const promise = api.list("stub")
            await expect(promise).to.eventually.eql([
                { id: 1, name: "sally" },
                { id: 2, name: "george" },
            ]);

            expect(fetch.done()).to.be.true;
            verify(middleware).called;
            fetch.reset();
        })

    })

    describe("CRUD", () => {

        it("makes a get request", async () => {
            fetch.getOnce("https://www.example.com/stub", { foo: "bar", flip: "flop" }, { headers: { "content-type": "application/json", "accept": "application/json" } });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.get("stub");
            await expect(promise).to.eventually.eql({ foo: "bar", flip: "flop" })
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes a list request", async () => {
            fetch.getOnce("https://www.example.com/stub", ["a", "b", "c"], { headers: { "content-type": "application/json", "accept": "application/json" } });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.list("stub");
            await expect(promise).to.eventually.eql(["a", "b", "c"]);
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes a list request with search parameters", async () => {
            fetch.getOnce("https://www.example.com/stub?search=cow", ["a", "b", "c"], { headers: { "content-type": "application/json", "accept": "application/json" } });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.list("stub", { search: "cow" });
            await expect(promise).to.eventually.eql(["a", "b", "c"]);
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes a list request with filter parameters", async () => {
            fetch.getOnce("https://www.example.com/stub?dice=rolled&bread=comforted", ["a", "b", "c"], { headers: { "content-type": "application/json", "accept": "application/json" } });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.list("stub", { filter: { "dice": "rolled", "bread": "comforted" } });
            await expect(promise).to.eventually.eql(["a", "b", "c"]);
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes a list request with search and filter parameters", async () => {
            fetch.getOnce("https://www.example.com/stub?search=cow&dice=rolled", ["a", "b", "c"], { headers: { "content-type": "application/json", "accept": "application/json" } });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.list("stub", { search: "cow", filter: { "dice": "rolled" } });
            await expect(promise).to.eventually.eql(["a", "b", "c"]);
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes a page request", async () => {
            fetch.getOnce(
                "https://www.example.com/stub?page=1",
                { page: { previous: null, current: 1, next: 2, last: 2 }, items: ["a", "b", "c"], meta: {} },
                { headers: { "content-type": "application/json", "accept": "application/json" } }
            );

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.page("stub");
            await expect(promise).to.eventually.eql({ page: { previous: null, current: 1, next: 2, last: 2 }, items: ["a", "b", "c"], meta: {} })
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes a second page request", async () => {
            fetch.getOnce(
                "https://www.example.com/stub?page=2",
                { page: { previous: 1, current: 2, next: null, last: 2 }, items: ["d", "e", "f"], meta: {} },
                { headers: { "content-type": "application/json", "accept": "application/json" } }
            );

            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.page("stub", 2);
            await expect(promise).to.eventually.eql({ page: { previous: 1, current: 2, next: null, last: 2 }, items: ["d", "e", "f"], meta: {} })
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes a create request", async () => {
            fetch.postOnce(
                "https://www.example.com/resource",
                { id: 1, name: "foo", description: "bar" },
                { headers: { "content-type": "application/json", "accept": "application/json" }, body: { name: "foo", description: "bar" } }
            );
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.create("resource", { name: "foo", description: "bar" })
            await expect(promise).to.eventually.eql({ id: 1, name: "foo", description: "bar" })
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes a retrieve request", async () => {
            fetch.getOnce(
                "https://www.example.com/resource/1",
                { id: 1, name: "foo", description: "bar" },
                { headers: { "content-type": "application/json", "accept": "application/json" } }
            );
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.retrieve("resource", 1);
            await expect(promise).to.eventually.eql({ id: 1, name: "foo", description: "bar" })
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes an update request", async () => {
            fetch.patchOnce(
                "https://www.example.com/resource/1",
                { id: 1, name: "foobuzz", description: "barflop" },
                { headers: { "content-type": "application/json", "accept": "application/json" }, body: { name: "foobuzz", description: "barflop" } }
            );
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.update("resource", 1, { name: "foobuzz", description: "barflop" })
            await expect(promise).to.eventually.eql({ id: 1, name: "foobuzz", description: "barflop" })
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })

        it("makes a delete request", async () => {
            fetch.deleteOnce(
                "https://www.example.com/resource/1",
                JSON.stringify({ status: "deleted" }),
                { headers: { "content-type": "application/json", "accept": "application/json" } }
            );
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const promise = api.delete("resource", 1);
            await expect(promise).to.eventually.eql({ status: "deleted" })
            expect(fetch.done()).to.be.true;
            fetch.reset();
        })
    })

    describe("Errors", () => {
        it("throws an authentication error on a HTTP 401 response", async () => {
            fetch.getOnce(
                "https://www.example.com/stub",
                {
                    status: 401,
                    body: { "detail": "You must supply a valid access token" }
                }
            );
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const authFailedHandler = sinon.spy();
            api.on("authentication-failed", authFailedHandler);

            const promise = api.list("stub");

            await expect(promise).to.eventually.be.rejectedWith("Unauthorized")
                .and.be.an.instanceOf(AuthenticationApiError)
                .and.have.property("data")
                .and.have.property("detail", "You must supply a valid access token");

            expect(fetch.done()).to.be.true;
            expect(authFailedHandler.calledOnce).to.be.true;
            fetch.reset();
        })

        it("throws a generic API error on a different HTTP response (e.g., 418)", async () => {
            fetch.getOnce(
                "https://www.example.com/stub",
                {
                    status: 418,
                    body: { "detail": "Here is my handle, and here is my spout" }
                }
            );
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, []);
            const requestFailedHandler = sinon.spy();
            api.on("request-failed", requestFailedHandler);

            const promise = api.list("stub");

            await expect(promise).to.eventually.be.rejectedWith("I'm a Teapot")
                .and.be.an.instanceOf(GenericApiError)
                .and.have.property("data")
                .and.have.property("detail", "Here is my handle, and here is my spout");

            expect(fetch.done()).to.be.true;
            expect(requestFailedHandler.calledOnce).to.be.true;
            fetch.reset();
        })

    })

})

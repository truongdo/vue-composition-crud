/**
 * Vue Composition CRUD Module
 * @module
 */

import { BaseApiError, GenericApiError, AuthenticationApiError } from "./api/error";
import { Api } from "./api/api";
import { CsrfTokenMiddleware } from "./api/middleware/csrfToken";
import { TokenAuthenticationMiddleware } from "./api/middleware/tokenAuthentication";
import { BearerTokenAuthenticationMiddleware } from "./api/middleware/bearerTokenAuthentication";
import { SessionAuthenticationMiddleware } from "./api/middleware/sessionAuthentication";
import { TrailingSlashMiddleware } from "./api/middleware/trailingSlash";
import { PageMiddleware } from "./api/middleware/page";
import { useApi, useGlobalApi } from "./composables/api";
import { useResource } from "./composables/resource";
import { useCollection } from "./composables/collection";
import { usePage } from "./composables/page";
import { useInfinitePage } from "./composables/infinitePage";

export {
    // API Errors
    BaseApiError,
    GenericApiError,
    AuthenticationApiError,

    // API Classes
    Api,

    // API Middlware
    CsrfTokenMiddleware,
    TokenAuthenticationMiddleware,
    BearerTokenAuthenticationMiddleware,
    SessionAuthenticationMiddleware,
    TrailingSlashMiddleware,
    PageMiddleware,

    // Composables
    useApi,
    useGlobalApi,
    useResource,
    useCollection,
    usePage,
    useInfinitePage,
}

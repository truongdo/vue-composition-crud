import { expect } from "chai"
import * as INDEX from "./index"

describe("Module Entrypoint", () => {

    it("exports the expected types", () => {
        expect(INDEX.BaseApiError).to.exist;
        expect(INDEX.GenericApiError).to.exist;
        expect(INDEX.AuthenticationApiError).to.exist;

        expect(INDEX.Api).to.exist;

        expect(INDEX.CsrfTokenMiddleware).to.exist;
        expect(INDEX.TokenAuthenticationMiddleware).to.exist;
        expect(INDEX.BearerTokenAuthenticationMiddleware).to.exist;
        expect(INDEX.SessionAuthenticationMiddleware).to.exist;
        expect(INDEX.TrailingSlashMiddleware).to.exist;
        expect(INDEX.PageMiddleware).to.exist;

        expect(INDEX.useApi).to.exist;
        expect(INDEX.useGlobalApi).to.exist;
        expect(INDEX.useResource).to.exist;
        expect(INDEX.useCollection).to.exist;
        expect(INDEX.usePage).to.exist;
        expect(INDEX.useInfinitePage).to.exist;
    })

})

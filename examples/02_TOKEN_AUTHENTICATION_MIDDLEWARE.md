
## Token Authentication Middleware

```js
import { Api, TokenAuthenticationMiddleware } from "vue-composition-crud";

const api = new Api(
    {
        baseUrl: "https://www.example.com/api/"
    },
    [
        new TokenAuthenticationMiddleware({
            passwordLoginPath: "login",
            refreshTokenPath: "token/refresh"
        })
    ]
);

const username = "my-username";
const password = "my-super-secret-password";

// POST https://www.example.com/api/login with JSON payload
// Throws AuthenticationApiError on failure
// Resulting {access, refresh} response is stored
// Access is supplied as a bearer token for each request
// Access token is automatically refreshed as needed
await api.authenticate("password", {username, password});

// GET https://www.example.com/api/current-user
const userInfo = await api.get("current-user");

// If you already have an access or access and bearer token combination,
// you can supply them directly to the api.
// Don't keep your tokens in localstore, it's not safe.
const access = "<supplied from somewhere>";
const reresh = "<supplied from somewhere>";
await api.authenticate("token", {access, refresh});

// If your system uses an API key rather than a bearer token, you can
// supply that too.
const key = "<some api key>";
await api.authenticate("api", {key});
```